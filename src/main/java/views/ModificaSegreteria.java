package views;

import domain.users.Admin;
import domain.users.Segreteria;
import main.Main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ModificaSegreteria extends JFrame {
    public ModificaSegreteria(Segreteria temp) {
        setVisible(true);
        setTitle("Modifica segreteria");
        setBounds(100, 100, 192, 177);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(3, 0, 0, 0));

        JButton mod_passw = new JButton("Modifica la password");
        mod_passw.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                Admin.scelta1=1;
                Admin.modificaSegreteria(temp);
                dispose();
            }
        });
        contentPane.add(mod_passw);

        JButton rimuovi = new JButton("Rimuovi la segreteria");
        rimuovi.setFont(new Font("Tahoma", Font.PLAIN, 10));
        rimuovi.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Admin.scelta1=2;
                Admin.modificaSegreteria(temp);
                dispose();
            }
        });
        contentPane.add(rimuovi);

        JButton indietro = new JButton("Torna al menu precedente");
        indietro.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                Main.menuAdmin();
                dispose();
            }
        });
        contentPane.add(indietro);

        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        setResizable(false);
    }

}