package views;

import main.Main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MenuIniziale extends JFrame {
    public MenuIniziale() {
        setTitle("Menu iniziale");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 192, 236);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(3, 0, 0, 0));

        JButton esci = new JButton("Esci dal programma");
        esci.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });
        contentPane.add(esci);

        JButton Login = new JButton("Login");
        Login.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                dispose();
                Main.login();
            }
        });
        contentPane.add(Login);

        JButton Reg_docente = new JButton("Registrati come docente");
        Reg_docente.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                dispose();
                Main.nuovoDocente();
            }
        });
        contentPane.add(Reg_docente);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        this.setResizable(false);
    }
}

