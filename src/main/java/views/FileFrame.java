package views;

import javax.swing.*;
import java.awt.*;

public class FileFrame extends JFrame {
    private FileDialog fd;

    public FileFrame() {
        super("Scegli il file Excel");
        fd = new FileDialog();
        getContentPane().add(fd);
        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
    }

    public FileDialog getFD() {
        return this.fd;
    }
}
