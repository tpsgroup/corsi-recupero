package views;

import main.Main;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

import static main.Main.menuAdmin;
import static main.Main.menuAdmin2;

public class MenuAdmin extends JFrame {

    private JPanel contentPane;

    public MenuAdmin() {
        setTitle("Menu admin");
        setBounds(100, 100, 192, 375);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(5, 0, 0, 0));

        JButton esci = new JButton("Esci dal programma");
        esci.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });
        contentPane.add(esci);

        JButton Cambia_Password = new JButton("Cambia Password");
        Cambia_Password.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt2=1;
                dispose();
                menuAdmin2();
            }
        });
        contentPane.add(Cambia_Password);

        JButton Aggiungi_Segreteria = new JButton("Aggiungi Segreteria");
        Aggiungi_Segreteria.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt2=2;
                dispose();
                menuAdmin2();
            }
        });
        contentPane.add(Aggiungi_Segreteria);

        JButton Modifica_Segreteria = new JButton("Modifica Segreteria");
        Modifica_Segreteria.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt2=3;
                dispose();
                menuAdmin2();
            }
        });
        contentPane.add(Modifica_Segreteria);

        JButton Modifica_Docente = new JButton("Modifica Docente");
        Modifica_Docente.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt2=4;
                dispose();
                menuAdmin2();
            }
        });
        contentPane.add(Modifica_Docente);
        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        setResizable(false);
    }

}
