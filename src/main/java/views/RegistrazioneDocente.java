package views;

import domain.Credenziali;
import domain.Giorno;
import enums.GiornoSettimana;
import enums.Materia;
import main.Main;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.util.ArrayList;

import static utils.Utils.trovaDocenteByEmail;
import static utils.Utils.trovaSegreteriaByEmail;

public class RegistrazioneDocente extends JFrame {

    JPanel contentPane;
    JLabel Materie;
    private final JTextField Nome;
    private final JTextField Cognome;
    private final JTextField Email;
    private final JTextField Password;
    JRadioButton si;
    JRadioButton No;
    private final boolean[] anno = new boolean[Giorno.NUMCORSI];
    private GiornoSettimana giorno;
    private byte corsi;
    JLabel Avviso = new JLabel("ATTENZIONE: Completare i campi in rosso");
    String title = "REGISTRAZIONE DOCENTE";
    boolean lunedì;
    private final ArrayList<Materia> mlist = new ArrayList<>();

    public RegistrazioneDocente() {
        setTitle(title);
        setBounds(100, 100, 991, 315);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel label = new JPanel(new GridLayout(9, 1,2, 2));
        label.setBounds(5, 5, 142, 251);
        label.add(new JLabel("Nome:", SwingConstants.LEFT));
        label.add(new JLabel("Cognome:", SwingConstants.LEFT));
        label.add(new JLabel("E-Mail:", SwingConstants.LEFT));
        label.add(new JLabel("Password:", SwingConstants.LEFT));
        label.add(new JLabel("Anno a cui insegnare:", SwingConstants.LEFT));
        label.add(new JLabel("Giorno settimanale libero:", SwingConstants.LEFT));
        label.add(new JLabel("Numero corsi settimanali:", SwingConstants.LEFT));
        label.add(new JLabel("Ha la sesta ora il lunedì?:", SwingConstants.LEFT));
        contentPane.add(label);

        JCheckBox Chimica = new JCheckBox("Chimica");
        Chimica.setBounds(307, 39, 97, 23);
        contentPane.add(Chimica);

        JCheckBox Diritto = new JCheckBox("Diritto");
        Diritto.setBounds(445, 39, 97, 23);
        contentPane.add(Diritto);

        JCheckBox Disegno_mecc = new JCheckBox("Disegno e progetto (meccanica)");
        Disegno_mecc.setBounds(736, 39, 233, 23);
        contentPane.add(Disegno_mecc);

        JCheckBox Elettronica = new JCheckBox("Elettronica");
        Elettronica.setBounds(588, 39, 97, 23);
        contentPane.add(Elettronica);

        JCheckBox Fisica = new JCheckBox("Fisica");
        Fisica.setBounds(307, 77, 97, 23);
        contentPane.add(Fisica);

        JCheckBox Info = new JCheckBox("Informatica");
        Info.setBounds(445, 77, 97, 23);
        contentPane.add(Info);

        JCheckBox Inglese = new JCheckBox("Inglese");
        Inglese.setBounds(588, 77, 97, 23);
        contentPane.add(Inglese);

        JCheckBox Italiano = new JCheckBox("Italiano");
        Italiano.setBounds(736, 77, 164, 23);
        contentPane.add(Italiano);

        JCheckBox Matematica = new JCheckBox("Matematica");
        Matematica.setBounds(307, 114, 97, 23);
        contentPane.add(Matematica);

        JCheckBox Meccanica = new JCheckBox("Meccanica");
        Meccanica.setBounds(445, 114, 97, 23);
        contentPane.add(Meccanica);

        JCheckBox Scienze = new JCheckBox("Scienze della terra");
        Scienze.setBounds(588, 114, 146, 23);
        contentPane.add(Scienze);

        JCheckBox Sistemi_elett = new JCheckBox("Sistemi (elettronica)");
        Sistemi_elett.setBounds(736, 114, 177, 23);
        contentPane.add(Sistemi_elett);

        JCheckBox Tecnologie_mecc = new JCheckBox("Tecnologie meccaniche");
        Tecnologie_mecc.setBounds(736, 152, 177, 23);
        contentPane.add(Tecnologie_mecc);

        JCheckBox Storia = new JCheckBox("Storia");
        Storia.setBounds(588, 152, 97, 23);
        contentPane.add(Storia);

        JCheckBox Sistemi_mecc = new JCheckBox("Sistemi (meccanica)");
        Sistemi_mecc.setBounds(445, 152, 141, 23);
        contentPane.add(Sistemi_mecc);

        JCheckBox Sistemi_info = new JCheckBox("Sistemi (informatica) ");
        Sistemi_info.setBounds(307, 152, 141, 23);
        contentPane.add(Sistemi_info);

        Avviso.setForeground(Color.RED);
        Avviso.setVisible(false);
        Avviso.setBounds(588, 242, 312, 23);
        contentPane.add(Avviso);

        si = new JRadioButton("SI");
        si.setBounds(157, 201, 43, 23);
        contentPane.add(si);

        No = new JRadioButton("NO");
        No.setBounds(207, 201, 67, 23);
        contentPane.add(No);

        ButtonGroup g= new ButtonGroup();
        g.add(si); g.add(No);

        JSpinner N_corsi = new JSpinner();
        N_corsi.setModel(new SpinnerNumberModel(1, 1, 6, 1));
        N_corsi.setBounds(161, 175, 73, 20);
        Object corsii= N_corsi.getValue();
        int corsi2 = (int) corsii;
        corsi = (byte) corsi2;
        contentPane.add(N_corsi);

        JCheckBox primo = new JCheckBox("1");
        JCheckBox secondo = new JCheckBox("2");
        JCheckBox terzo = new JCheckBox("3");
        JCheckBox quarto = new JCheckBox("4");
        primo.setBounds(161, 120, 31, 23); contentPane.add(primo);
        secondo.setBounds(192, 120, 31, 23); contentPane.add(secondo);
        terzo.setBounds(223, 120, 31, 23); contentPane.add(terzo);
        quarto.setBounds(254, 120, 31, 23); contentPane.add(quarto);

        JComboBox Giorno = new JComboBox(new String[] {"LUNEDI", "MARTEDI", "MERCOLEDI", "GIOVEDI", "VENERDI", "SABATO"});
        Giorno.setBounds(161, 147, 127, 23);
        contentPane.add(Giorno);

        Nome = new JTextField();
        Nome.setBounds(161, 11, 127, 20);
        contentPane.add(Nome);
        Nome.setColumns(10);

        Cognome = new JTextField();
        Cognome.setColumns(10);
        Cognome.setBounds(161, 39, 127, 20);
        contentPane.add(Cognome);

        Email = new JTextField("@itisandria.it");
        Email.setColumns(10);
        Email.setBounds(161, 66, 127, 20);
        contentPane.add(Email);

        Password = new JTextField();
        Password.setColumns(10);
        Password.setBounds(161, 92, 127, 20);
        contentPane.add(Password);

        JCheckBox TPS_elett = new JCheckBox("TPS (elettronica)");
        TPS_elett.setBounds(445, 191, 141, 23);
        contentPane.add(TPS_elett);

        JCheckBox Telec = new JCheckBox("Telecomunicazioni");
        Telec.setBounds(307, 191, 127, 23);
        contentPane.add(Telec);

        JCheckBox TPS_info = new JCheckBox("TPS (informatica)");
        TPS_info.setBounds(588, 191, 146, 23);
        contentPane.add(TPS_info);

        JCheckBox tecnologie_e_tecniche = new JCheckBox("Tecnologie e tecniche di rapp.grafica");
        tecnologie_e_tecniche.setBounds(736, 191, 233, 23);
        contentPane.add(tecnologie_e_tecniche);

        JButton INVIA = new JButton("INVIA");
        INVIA.addActionListener(e -> {//controllo su tutti i campi
            Avviso.setVisible(false);
            Password.setBorder(new LineBorder(Color.BLACK));
            Nome.setBorder(new LineBorder(Color.BLACK));
            Cognome.setBorder(new LineBorder(Color.BLACK));
            Email.setBorder(new LineBorder(Color.BLACK));
            si.setForeground(Color.WHITE);
            No.setForeground(Color.WHITE);
            Materie.setForeground(Color.WHITE);
            if(si.isSelected()){
                lunedì=true;
            }
            if(No.isSelected()){
                lunedì=false;
            }
            anno[0] = primo.isSelected();
            anno[1] = secondo.isSelected();
            anno[2] = terzo.isSelected();
            anno[3] = quarto.isSelected();
            if(Chimica.isSelected()){ mlist.add(Materia.CHIMICA);}
            if(Diritto.isSelected()){mlist.add(Materia.DIRITTO);}
            if(Disegno_mecc.isSelected()){ mlist.add(Materia.DISEGNO_PROGETTO);}
            if(Elettronica.isSelected()){ mlist.add(Materia.ELETTRONICA);}
            if(Fisica.isSelected()){ mlist.add(Materia.FISICA);}
            if(Info.isSelected()){ mlist.add(Materia.INFORMATICA);}
            if(Inglese.isSelected()){ mlist.add(Materia.INGLESE);}
            if(Italiano.isSelected()){ mlist.add(Materia.ITALIANO);}
            if(Matematica.isSelected()){ mlist.add(Materia.MATEMATICA);}
            if(Meccanica.isSelected()){ mlist.add(Materia.MECCANICA);}
            if(Scienze.isSelected()){ mlist.add(Materia.SCIENZEDELLATERRA);}
            if(Sistemi_elett.isSelected()){ mlist.add(Materia.SISTEMI_ELETTRONICA);}
            if(Sistemi_info.isSelected()){ mlist.add(Materia.SISTEMI_INFORMATICA);}
            if(Sistemi_mecc.isSelected()){ mlist.add(Materia.SISTEMI_MECCANICA);}
            if(Storia.isSelected()){ mlist.add(Materia.STORIA);}
            if(Tecnologie_mecc.isSelected()){ mlist.add(Materia.TECNOLOGIE_MECCANICHE);}
            if(Telec.isSelected()){ mlist.add(Materia.TELECOMUNICAZIONI);}
            if(TPS_elett.isSelected()){ mlist.add(Materia.TPS_ELETTRONICA);}
            if(TPS_info.isSelected()){ mlist.add(Materia.TPS_INFORMATICA);}
            if(tecnologie_e_tecniche.isSelected()){ mlist.add(Materia.TTRG);}
            boolean c = controlloForm();
            Object corsi22 = N_corsi.getValue();
            corsi = (byte) ((int) corsi22);
            int giornoo = Giorno.getSelectedIndex();
            giorno =  GiornoSettimana.values()[giornoo];
            if(!c)
            {
                Main.nuovoDocente2();
                dispose();
            }else {
                Avviso.setVisible(true);
            }
        });
        INVIA.setBounds(399, 234, 154, 38);
        contentPane.add(INVIA);

        JButton INDIETRO = new JButton("INDIETRO");
        INDIETRO.setBounds(150, 234, 154, 38);
        INDIETRO.addActionListener(e -> { dispose(); new MenuIniziale();});
        contentPane.add(INDIETRO);

        Materie = new JLabel("Materie:", SwingConstants.LEFT);
        Materie.setFont(new Font("Tahoma", Font.BOLD, 18));
        Materie.setBounds(526, 6, 177, 26);
        contentPane.add(Materie);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        this.setResizable(false);
    }

    private boolean controlloForm() {
        boolean c=false;
        String nome = Nome.getText();
        String cognome = Cognome.getText();
        String email = Email.getText();
        String password = Password.getText();
        if(mlist.size() < 1) {
            Materie.setForeground(Color.RED);
            c=true;
        }
        if(nome.length()<1) {
            Nome.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(cognome.length()<1) {
            Cognome.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(email.length()<1) {
            Email.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(password.length()<1) {
            Password.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(!si.isSelected() && !No.isSelected()){
            si.setForeground(Color.RED);
            No.setForeground(Color.RED);
            c=true;
        }
        if(trovaDocenteByEmail(email)!=null || trovaSegreteriaByEmail(email)!=null) {
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
            JOptionPane.showMessageDialog(this, "Utente gia' registrato.");
            new MenuIniziale();
        }
        if(!Credenziali.isValid(email)) {
            JOptionPane.showMessageDialog(this, "Inserire una email valida di dominio '@itisandria.it'");
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
        }
        return c;
    }

    public JTextField getNome() {
        return Nome;
    }

    public JTextField getCognome() {
        return Cognome;
    }

    public JTextField getEmail() {
        return Email;
    }

    public JTextField getPassword(){
        return Password;
    }

    public boolean[] getAnno() {return anno; }

    public GiornoSettimana getGiorno(){ return giorno; }

    public byte getCorsi(){ return corsi; }
    public void setCorsi(byte corsi){ this.corsi = corsi; }

    public boolean getLunedi(){ return lunedì; }

    public ArrayList getMaterie(){ return mlist;}
}
