package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class LoginFrame extends JFrame {
    private JTextField tfEmail;
    private JPasswordField pfPsw;
    private boolean sig_indietro;

    public LoginFrame(String title) {
        super();
        JPanel panel = new JPanel(new BorderLayout(5, 5));
        sig_indietro = false;
        JPanel label = new JPanel(new GridLayout(0, 1, 2, 2));
        label.add(new JLabel("E-Mail:", SwingConstants.LEFT));
        label.add(new JLabel("Password:", SwingConstants.LEFT));
        label.add(new JPanel());
        panel.add(label, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
        tfEmail = new JTextField("@itisandria.it", 20);
        controls.add(tfEmail);
        pfPsw = new JPasswordField(20);
        controls.add(pfPsw);

        JPanel pShowPsw = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pShowPsw.add(new JLabel("Mostra Password"));
        JCheckBox hidePasswordCheckbox = new JCheckBox();
        hidePasswordCheckbox.addItemListener(new ItemListener() {
            final char echoChar = pfPsw.getEchoChar();
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    pfPsw.setEchoChar((char) 0);
                } else {
                    pfPsw.setEchoChar(echoChar);
                }
            }
        });
        pShowPsw.add(hidePasswordCheckbox);
        controls.add(pShowPsw);
        panel.add(controls, BorderLayout.CENTER);

        JButton indietro = new JButton("Indietro");
        indietro.addActionListener(e -> {
            sig_indietro = true; dispose(); new MenuIniziale();
        });
        JPanel indPnl = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        indPnl.add(indietro);
        panel.add(indPnl, BorderLayout.SOUTH);

        this.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        this.setResizable(false);
        this.setVisible(true);
        JOptionPane.showMessageDialog(this, panel, title, JOptionPane.QUESTION_MESSAGE);
    }

    public JTextField getEmail() {
        return tfEmail;
    }

    public JPasswordField getPassword(){
        return pfPsw;
    }

    public boolean isIndietro() {
        return sig_indietro;
    }
}
