package views;

import main.Main;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static main.Main.menuSegreteria2;

public class MenuSegreteria extends JFrame {

    public MenuSegreteria() {
        setTitle("Menu segreteria");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setBounds(100, 100, 192, 375);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(5, 0, 0, 0));

        JButton esci = new JButton("Esci dal programma");
        esci.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt=0;
                menuSegreteria2();
            }
        });
        contentPane.add(esci);

        JButton Importa = new JButton("Importa corsisti da Excel");
        Importa.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt=1;
                dispose();
                menuSegreteria2();
            }
        });
        contentPane.add(Importa);

        JButton Genera_corsi = new JButton("Genera corsi");
        Genera_corsi.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt=2;
                dispose();
                menuSegreteria2();
            }
        });
        contentPane.add(Genera_corsi);

        JButton Genera_calendario = new JButton("Genera calendario");
        Genera_calendario.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt=3;
                dispose();
                menuSegreteria2();
            }
        });
        contentPane.add(Genera_calendario);

        JButton Visualizza_corso = new JButton("Visualizza corso via ID");
        Visualizza_corso.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt=4;
                dispose();
                menuSegreteria2();
            }
        });
        contentPane.add(Visualizza_corso);
        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        this.setResizable(false);
    }
}
