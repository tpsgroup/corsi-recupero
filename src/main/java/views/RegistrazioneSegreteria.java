package views;

import domain.Credenziali;
import domain.users.Admin;
import main.Main;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import static utils.Utils.trovaDocenteByEmail;
import static utils.Utils.trovaSegreteriaByEmail;

public class RegistrazioneSegreteria extends JFrame {

    private JPanel contentPane;
    private JTextField Nome;
    private JTextField Cognome;
    private JTextField Password;
    private JTextField Email;
    JLabel Avviso;


    public RegistrazioneSegreteria() {
        setTitle("Registra segreteria");
        setBounds(100, 100, 350, 197);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);


        JPanel label = new JPanel(new GridLayout(4, 1,2, 2));
        label.setBounds(5, 5, 70, 107);
        label.add(new JLabel("Nome:", SwingConstants.LEFT));
        label.add(new JLabel("Cognome:", SwingConstants.LEFT));
        label.add(new JLabel("E-Mail:", SwingConstants.LEFT));
        contentPane.add(label);
        JLabel label_1 = new JLabel("Password:", SwingConstants.LEFT);
        label.add(label_1);

        Nome = new JTextField();
        Nome.setColumns(15);
        Nome.setBounds(74, 5, 200, 20);
        contentPane.add(Nome);

        Cognome = new JTextField();
        Cognome.setColumns(15);
        Cognome.setBounds(74, 33, 200, 20);
        contentPane.add(Cognome);

        Password = new JTextField();
        Password.setColumns(15);
        Password.setBounds(74, 89, 200, 20);
        contentPane.add(Password);

        Email = new JTextField("@itisandria.it");
        Email.setColumns(15);
        Email.setBounds(74, 61, 200, 20);
        contentPane.add(Email);

        Avviso = new JLabel("Completare i campi in rosso");
        Avviso.setForeground(Color.RED);
        Avviso.setVisible(false);
        Avviso.setBounds(10, 150, 191, 14);
        contentPane.add(Avviso);

        RegistrazioneSegreteria thisReference = this;
        JButton INVIA = new JButton("INVIA");
        INVIA.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Avviso.setVisible(false);
                Password.setBorder(new LineBorder(Color.BLACK));
                Nome.setBorder(new LineBorder(Color.BLACK));
                Cognome.setBorder(new LineBorder(Color.BLACK));
                Email.setBorder(new LineBorder(Color.BLACK));
                boolean c = controlloForm();
                if(!c)
                {
                    Admin.aggiungiSegreteria(Main.segrFile, thisReference);
                    dispose();
                }else {
                    Avviso.setVisible(true);
                }

            }
        });
        INVIA.setBounds(120, 120, 80, 27);
        contentPane.add(INVIA);

        JButton INDIETRO = new JButton("INDIETRO");
        INDIETRO.setBounds(20, 120, 90, 27);
        INDIETRO.addActionListener(e -> { dispose(); Main.menuAdmin();});
        contentPane.add(INDIETRO);

        //pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        setResizable(false);
    }

    private boolean controlloForm() {
        boolean c=false;
        String nome = Nome.getText();
        String cognome = Cognome.getText();
        String email = Email.getText();
        String password = Password.getText();
        if(nome.length()<1) {
            Nome.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(cognome.length()<1) {
            Cognome.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(email.length()<1) {
            Email.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(password.length()<1) {
            Password.setBorder(new LineBorder(Color.RED));
            c=true;
        }
        if(trovaDocenteByEmail(email)!=null || trovaSegreteriaByEmail(email)!=null) {
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
            JOptionPane.showMessageDialog(this, "Utente gia' registrato.");
        }
        if(!Credenziali.isValid(email)) {
            JOptionPane.showMessageDialog(this, "Inserire una email valida di dominio '@itisandria.it'");
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
        }
        if(trovaDocenteByEmail(email)!=null || trovaSegreteriaByEmail(email)!=null) {
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
            JOptionPane.showMessageDialog(this, "Utente gia' registrato.");
        }
        if(!Credenziali.isValid(email)) {
            JOptionPane.showMessageDialog(this, "Inserire una email valida di dominio '@itisandria.it'");
            c=true;
            Email.setBorder(new LineBorder(Color.RED));
        }
        return c;
    }
    public JTextField getNome() {
        return Nome;
    }
    public JTextField getCognome() {
        return Cognome;
    }
    public JTextField getEmail() {
        return Email;
    }
    public JTextField getPassword(){
        return Password;
    }
}

