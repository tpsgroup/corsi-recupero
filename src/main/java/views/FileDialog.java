package views;

import utils.FileTypeFilter;
import utils.Utils;

import java.io.File;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JPanel;

public class FileDialog extends JPanel {
    private File file;

    public FileDialog() {
        try {
            JFileChooser fileChooser = new JFileChooser();
            ArrayList<String> extensions = new ArrayList<>();
            extensions.add(Utils.xlsx);
            extensions.add(Utils.xls);
            FileTypeFilter xlsx = new FileTypeFilter(extensions, "Fogli di calcolo ");
            fileChooser.setFileFilter(xlsx);
            int n = fileChooser.showOpenDialog(FileDialog.this);
            if (n == JFileChooser.APPROVE_OPTION) {
                this.file = fileChooser.getSelectedFile();
            }
        } catch (Exception ignored) {}
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
