package views;

import main.Main;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;

import static main.Main.*;

public class MenuDocente extends JFrame {

    private JPanel contentPane;

    public MenuDocente() {
        setVisible(true);
        setTitle("Menu Docente");
        setBounds(100, 100, 192, 177);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new GridLayout(2, 0, 0, 0));

        JButton esci = new JButton("Esci dal programma");
        esci.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                System.exit(0);
            }
        });
        contentPane.add(esci);

        JButton Esporta = new JButton("Esporta calendario personale");
        Esporta.setFont(new Font("Tahoma", Font.PLAIN, 10));
        Esporta.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                Main.opt3=1;
                //dispose();
                menuDocente2();
                //setVisible(true);
            }
        });
        contentPane.add(Esporta);
        pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        setVisible(true);
        setResizable(false);
    }

}