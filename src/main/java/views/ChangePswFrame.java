package views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ChangePswFrame extends JFrame {
    private final JPasswordField vecchiapsw;
    private JPasswordField nuovapsw;
    private JPasswordField ripetipsw;

    public ChangePswFrame(String title) {
        super();

        JPanel panel = new JPanel(new BorderLayout(5, 5));

        JPanel label = new JPanel(new GridLayout(0, 1, 2, 2));
        label.add(new JLabel("Vecchia password:", SwingConstants.LEFT));
        label.add(new JLabel("Nuova password:", SwingConstants.LEFT));
        label.add(new JLabel("Ripeti password:", SwingConstants.LEFT));
        label.add(new JPanel());
        panel.add(label, BorderLayout.WEST);

        JPanel controls = new JPanel(new GridLayout(0, 1, 2, 2));
        vecchiapsw = new JPasswordField(20);
        controls.add(vecchiapsw);
        nuovapsw = new JPasswordField(20);
        controls.add(nuovapsw);
        ripetipsw = new JPasswordField(20);
        controls.add(ripetipsw);

        JPanel pShowPsw = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        pShowPsw.add(new JLabel("Mostra Password"));
        JCheckBox hidePasswordCheckbox = new JCheckBox();
        hidePasswordCheckbox.addItemListener(new ItemListener() {
            final char echoChar = vecchiapsw.getEchoChar();
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    vecchiapsw.setEchoChar((char) 0);
                    nuovapsw.setEchoChar((char) 0);
                    ripetipsw.setEchoChar((char) 0);
                } else {
                    vecchiapsw.setEchoChar(echoChar);
                    nuovapsw.setEchoChar(echoChar);
                    ripetipsw.setEchoChar(echoChar);
                }
            }
        });
        pShowPsw.add(hidePasswordCheckbox);
        controls.add(pShowPsw);

        panel.add(controls, BorderLayout.CENTER);

        this.pack();
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation((d.width - getWidth())/2 , (d.height - getHeight())/2);
        this.setResizable(false);
        this.setVisible(true);

        JOptionPane.showMessageDialog(this, panel, title, JOptionPane.QUESTION_MESSAGE);
    }

    public JPasswordField getVecchiapsw() {
        return vecchiapsw;
    }

    public JPasswordField getNuovapsw() {
        return nuovapsw;
    }

    public JPasswordField getRipetipsw() {
        return ripetipsw;
    }
}
