package domain;

import java.util.regex.Pattern;

public class Credenziali {
	private String email;
	private String password;

	public Credenziali(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public Credenziali() {
		email = password = null;
	}

	public boolean confermaPassword(String password) {
		if(password == null)
			throw new NullPointerException();
		return this.password.equals(password);
	}

	public static boolean isValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_.+-]+@(?:(?:[a-zA-Z0-9-]+\\.)?[a-zA-Z]+\\.)?(itisandria)\\.it$";

		Pattern pat = Pattern.compile(emailRegex);
		if (email == null)
			return false;
		return pat.matcher(email).matches();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}