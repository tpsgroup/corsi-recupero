package domain;

import domain.users.Utente;
import enums.Anno;
import enums.Materia;
import enums.Sezione;

public class Corsista extends Utente {
    private Sezione sezione;
    private Anno anno;
    private Materia[] materie;
    public static byte MAXCORSI = 3;

    public Corsista(String nome, String cognome, String email, Sezione sezione,
                    Anno anno, Materia[] materie) {
        super(Type.CORSISTA, nome, cognome, new Credenziali(email, null));
        this.sezione = sezione;
        this.anno = anno;
        this.materie = materie;
    }

    public Sezione getSezione() {
        return sezione;
    }

    public void setSezione(Sezione sezione) {
        this.sezione = sezione;
    }

    public Anno getAnno() {
        return anno;
    }

    public void setAnno(Anno anno) {
        this.anno = anno;
    }

    public Materia[] getMaterie() {
        return materie;
    }

    public void setMaterie(Materia[] materie) {
        this.materie = materie;
    }

    public String toString(){
        return this.getCognome() + " " + this.getNome() + ", " +
                (this.getAnno().ordinal() + 1) + this.getSezione().name() +
                " [" + this.getCredenziali().getEmail() + "]";
    }
}
