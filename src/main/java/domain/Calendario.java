package domain;

import enums.GiornoSettimana;

public class Calendario {
	public static final byte NUMGIORNI = 6;
    public static final byte NUMLEZIONI = 5;
    private Giorno[] giorni;

	public Calendario() {
		giorni = new Giorno[NUMGIORNI];
		for(int i=0; i < NUMGIORNI; ++i)
			giorni[i] = new Giorno(GiornoSettimana.values()[i]);
	}

	public Calendario(Giorno[] giorni) {
		this.giorni = giorni;
	}

	public Giorno[] getGiorni() {
		return this.giorni;
	}

	public void setGiorni(Giorno[] giorni) {
		this.giorni = giorni;
	}
}