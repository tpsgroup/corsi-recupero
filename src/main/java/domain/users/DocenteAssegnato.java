package domain.users;

import domain.Calendario;
import domain.Credenziali;
import enums.GiornoSettimana;
import enums.Materia;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DocenteAssegnato extends Docente {
	private boolean[] giorniOccupati; // la giornata libera sarà considerata già occupata

	public DocenteAssegnato(String nome, String cognome, Credenziali credenziali,
                            ArrayList<Materia> materie, boolean[] annoCorso,
                            GiornoSettimana freeDay, byte nCorsi, boolean sestaLun) {
		super(nome, cognome, credenziali, materie, annoCorso, freeDay, nCorsi, sestaLun);
		this.giorniOccupati = new boolean[Calendario.NUMGIORNI];
		this.giorniOccupati[freeDay.ordinal()] = true;
	}

	public DocenteAssegnato() {
		super();
	}

	public DocenteAssegnato(Docente docente) {
		super(docente.getNome(), docente.getCognome(), docente.getCredenziali(),
				docente.getMaterie(), docente.getAnnoCorso(), docente.getFreeDay(),
				docente.getnCorsi(), docente.getSestaLun());
		this.giorniOccupati = new boolean[Calendario.NUMGIORNI];
		this.giorniOccupati[freeDay.ordinal()] = true;
	}

	public void esportaCalendario(){
		try {
			JOptionPane.showMessageDialog(null,"Premere 'Ok' e attendere...");
			Workbook inworkbook = WorkbookFactory.create(new File("Calendario.xlsx"));
			Sheet insheet = inworkbook.getSheetAt(0);
			XSSFWorkbook outworkbook = new XSSFWorkbook();
			XSSFSheet outsheet = outworkbook.createSheet("Calendario");
			Row outfirstRow = outsheet.createRow(0);
			outfirstRow.createCell(0).setCellValue("N°");
			outfirstRow.createCell(1).setCellValue("ID");
			outfirstRow.createCell(2).setCellValue("DISCIPLINA");
			outfirstRow.createCell(3).setCellValue("DOCENTE");
			outfirstRow.createCell(4).setCellValue("N° ALUNNI");
			outfirstRow.createCell(5).setCellValue("ANNO");
			outfirstRow.createCell(6).setCellValue("CLASSI");
			outfirstRow.createCell(7).setCellValue("GIORNO");
			for(int i=0; i < Calendario.NUMLEZIONI; ++i){
				outfirstRow.createCell(8 + (i*2)).setCellValue("LEZIONE " + (i+1));
				outfirstRow.createCell(9 + (i*2));
			}
			int iCorsi=1;
			for(Row row : insheet){
				if(row.getCell(3).getStringCellValue().equals(this.getCognome())){
					Row outRow = outsheet.createRow(iCorsi);
					outRow.createCell(0).setCellValue(iCorsi);
					outRow.createCell(1).setCellValue(row.getCell(1).getNumericCellValue());
					outRow.createCell(2).setCellValue(row.getCell(2).getStringCellValue());
					outRow.createCell(3).setCellValue(row.getCell(3).getStringCellValue());
					outRow.createCell(4).setCellValue(row.getCell(4).getNumericCellValue());
					outRow.createCell(5).setCellValue(row.getCell(5).getStringCellValue());
					outRow.createCell(6).setCellValue(row.getCell(6).getStringCellValue());
					outRow.createCell(7).setCellValue(row.getCell(7).getStringCellValue());
					for(int i=0; i < Calendario.NUMLEZIONI; ++i){
						outRow.createCell(8 + (i*2)).setCellValue(row.getCell(8 + (i*2)).getStringCellValue());
						outRow.createCell(9 + (i*2)).setCellValue(row.getCell(9 + (i*2)).getStringCellValue());
					}
					++iCorsi;
				}
			}
			String nomeOut= this.getCognome() + "_" + "Calendario.xlsx";
			inworkbook.close();
			try (FileOutputStream outputStream = new FileOutputStream(nomeOut)) {
				outworkbook.write(outputStream);
			}
			JOptionPane.showMessageDialog(null,"Calendario dei corsi generato in '" + nomeOut + "'.");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Errore nella generazione del Calendario.\n" +
					"Assicurarsi che 'Calendario.xlsx' sia presente.\n" +
					"Se non c'è,contattare la segreteria per la generazione.");
		}
	}

	public boolean[] getGiorniOccupati(){
		return this.giorniOccupati;
	}

	public void setGiorniOccupati(boolean[] giorniOccupati){
		this.giorniOccupati = giorniOccupati;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}