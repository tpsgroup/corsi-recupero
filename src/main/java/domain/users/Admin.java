package domain.users;

import domain.Calendario;
import domain.Credenziali;
import domain.Giorno;
import enums.GiornoSettimana;
import enums.Materia;
import main.Main;
import utils.FileMan;
import utils.RWDomain;
import views.ChangePswFrame;
import views.ModificaSegreteria;
import views.RegistrazioneSegreteria;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

import static main.Main.*;

public class Admin extends Utente {
	public static byte scelta1;

	public Admin(String nome, String cognome, Credenziali credenziali) {
		super(Type.ADMIN, nome, cognome, credenziali);
	}

	public static void aggiungiSegreteria(FileMan f, RegistrazioneSegreteria r) {
		String nome= r.getNome().getText();
		String cognome= r.getCognome().getText();
		String email= r.getEmail().getText();
		String pwd= r.getPassword().getText();
		try {
			Segreteria s = new Segreteria(nome, cognome, new Credenziali(email, pwd));
			f.openOut(true);
			if(!RWDomain.writeSegreteria(f, s))
				throw new IOException();
			JOptionPane.showMessageDialog(null, "Segreteria registrata correttamente.");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Errore nella scrittura nel file. Registrazione annullata.");
		}
		f.close();
		menuAdmin();
	}

	public static void rimuoviSegreteria(String email) {
		boolean found = false; Segreteria temp;
		try {
			FileMan fout = new FileMan(UUID.randomUUID() + ".txt");
			fout.openOut(false);
			Main.segrFile.openIn();
			while((temp = RWDomain.readSegreteria(Main.segrFile)) != null) {
				if (!found) {
					if (temp.getCredenziali().getEmail().equals(email)) {
						found = true;
						continue;
					}
				}
				RWDomain.writeSegreteria(fout, temp);
			}
			fout.close(); Main.segrFile.close();
			Main.segrFile.getFile().delete();
			File f1 = new File(Main.segrFile.getNome());
			fout.getFile().renameTo(f1);
			Main.segrFile.resetFile(Main.segrFile.getNome());
		} catch (IOException ignored) {		}
	}

	public void rimuoviDocente(String email) {
		Docente temp;
		try {
			FileMan fout = new FileMan(UUID.randomUUID() + ".txt");
			fout.openOut(false);
			Main.docentiFile.openIn();
			while((temp = RWDomain.readDocente(Main.docentiFile)) != null) {
				if (temp.getCredenziali().getEmail().equals(email)) {
					continue;
				}
				RWDomain.writeDocente(fout, temp);
			}
			fout.close(); Main.docentiFile.close();
			Main.docentiFile.getFile().delete();
			File f1 = new File(Main.docentiFile.getNome());
			fout.getFile().renameTo(f1);
			Main.docentiFile.resetFile(Main.docentiFile.getNome());
		} catch (IOException ignored) { }
	}

	public boolean modificaDocente(String email){
		boolean found = false; Docente temp, docente = null;
		try {
			docentiFile.openIn();
			while ((temp = RWDomain.readDocente(docentiFile)) != null) {
				if (temp.getCredenziali().getEmail().equals(email)) {
					found = true;
					docente = temp;
					break;
				}
			}
			docentiFile.close();
			if(!found)
				JOptionPane.showMessageDialog(null,
						"Non è stato trovato alcun docente corrispondente all'email inserita.");
			else{
				Object sceltaObj; String sceltaStr;
				Object[] selezioni = {"1. Modifica anno del corso", "2. Modifica giorno libero",
						"3. Modifica numero corsi a settimana", "4. Modifica sesta ora",
						"5. Modificare la password", "6. Reinserire le materie", "7. Rimuovere il docente",
						"8. Tornare al menu' precedente" };
				sceltaObj = JOptionPane.showInputDialog(null, "Scegli cosa fare",
						"Modifica docente",JOptionPane.INFORMATION_MESSAGE,null, selezioni, selezioni[0]);
				sceltaStr = (String) sceltaObj;
				char c; c = sceltaStr.charAt(0);
				int scelta = Character.getNumericValue(c);
				modificaDocente(docente, scelta);
			}
			menuAdmin();
			return found;
		} catch (IOException e) {
			return false;
		}
	}

	public void modificaDocente(Docente docente, int scelta) throws IOException {
		byte gl,nc; Materia m;
		ArrayList<Materia> mlist = new ArrayList<>();
		boolean[] ac = new boolean[Giorno.NUMCORSI];
		rimuoviDocente(docente.getCredenziali().getEmail());
		switch (scelta) {
			case 1: {
				JCheckBox primo = new JCheckBox("1");
				JCheckBox secondo = new JCheckBox("2");
				JCheckBox terzo = new JCheckBox("3");
				JCheckBox quarto = new JCheckBox("4");
				JPanel pnl = new JPanel(new FlowLayout());
				pnl.add(primo); pnl.add(secondo); pnl.add(terzo); pnl.add(quarto);
				JOptionPane.showMessageDialog(null, pnl, "Modifica anno", JOptionPane.QUESTION_MESSAGE);
				ac[0] = primo.isSelected();
				ac[1] = secondo.isSelected();
				ac[2] = terzo.isSelected();
				ac[3] = quarto.isSelected();
				docente.annoCorso = ac;
				//JOptionPane.showMessageDialog(null, "L'anno attuale del corso è':" + anno);
				JOptionPane.showMessageDialog(null, "Modifica effettuata");
				break;
			}
			case 2: {
				byte giorno;
				Object[] selezioniGiorno = {"1. LUNEDI", "2. MARTEDI", "3. MERCOLEDI",
						"4. GIOVEDI", "5. VENEDI", "6. SABATO"};
				Object giornoObj;
				giornoObj = JOptionPane.showInputDialog(null, "Scegli il nuovo giorno",
						"Modifica giorno", JOptionPane.INFORMATION_MESSAGE, null,
						selezioniGiorno, selezioniGiorno[0]);
				String giornoStr = (String) giornoObj;
				char g = giornoStr.charAt(0);
				int giornoi = Character.getNumericValue(g);
				gl = (byte) giornoi;
				giorno = gl;
				if (gl >= 1 && gl <= Calendario.NUMGIORNI) {
					docente.freeDay = GiornoSettimana.values()[--gl];	//deve andare da 0 per essere utilizzato nel vettore
				}
				JOptionPane.showMessageDialog(null, "L'attuale giorno libero è':" + giorno);
				break;
			}
			case 3: {
				Object[] selezioniNCorsi = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
				Object nCorsiObj;
				nCorsiObj = JOptionPane.showInputDialog(null,
						"Scegli il nuovo numero di corsi settimanali", "Modifica n° corsi",
						JOptionPane.INFORMATION_MESSAGE, null, selezioniNCorsi, selezioniNCorsi[0]);
				int nCorsii = (int) nCorsiObj;
				nc = (byte) nCorsii;
				if (nc >= 1 && nc <= Calendario.NUMGIORNI) {
					docente.nCorsi = nc;
				}
				JOptionPane.showMessageDialog(null, "L'attuale numero di corsi settimanali è':" + nc);
				break;
			}
			case 4: {
				docente.sestaLun = !docente.sestaLun;
				if (docente.sestaLun)
					JOptionPane.showMessageDialog(null, "Ora il docente ha la sesta ora occupata.");
				else
					JOptionPane.showMessageDialog(null, "Ora il docente ha la sesta ora libera.");
				break;
			}
			case 5: {
				ChangePswFrame pswf = new ChangePswFrame("Corsi di recupero - Cambia password");
				pswf.setVisible(false);
				String vecchia = String.valueOf(pswf.getVecchiapsw().getPassword()),
						nuova = String.valueOf(pswf.getNuovapsw().getPassword()),
						ripeti = String.valueOf(pswf.getRipetipsw().getPassword());
				if (vecchia.equals("") || nuova.equals("")) {
					JOptionPane.showMessageDialog(null, "Almeno uno dei campi è vuoto, modifica password annullato.");
					break;
				}
				if (!vecchia.equals(docente.getCredenziali().getPassword())) {
					JOptionPane.showMessageDialog(null, "La vecchia password è errata, modifica password annullato.");
					break;
				}
				if (!nuova.equals(ripeti)) {
					JOptionPane.showMessageDialog(null, "Le due nuove password non corrispondono, modifca password annullato.");
					break;
				}
				if (vecchia.equals(nuova)) {
					JOptionPane.showMessageDialog(null, "La vecchia e la nuova password sono identiche, modifca password annullato.");
					break;
				}
				docente.setCredenziali(docente.getCredenziali().getEmail(), nuova);
				JOptionPane.showMessageDialog(null, "Password modificata correttamente.");
				break;
			}
			case 6:
				while(true){
					while((m = Materia.scegliMateria()) != null)
						mlist.add(m);
					if(mlist.size() > 0)
						break;
					JOptionPane.showMessageDialog(null,"Inserire almeno una materia.");
				}
				docente.setMaterie(mlist);
				break;
			case 7:
				JOptionPane.showMessageDialog(null,"Rimozione effettuata.");
				break;
			case 8:
				//System.out.println(temp);
				break;
			default:
				JOptionPane.showMessageDialog(null,"Scelta errata.");
				break;
		}
		if(scelta != 7){
			docentiFile.openOut(true);
			RWDomain.writeDocente(docentiFile, docente);
			docentiFile.close();
		}
	}

	public static void modificaSegreteria(Segreteria temp) {
		switch (scelta1) {
			case 1:
				ChangePswFrame pswf = new ChangePswFrame("Corsi di recupero - Cambia password"); pswf.setVisible(false);
				try{
					String vecchia = String.valueOf(pswf.getVecchiapsw().getPassword()),
							nuova = String.valueOf(pswf.getNuovapsw().getPassword()),
							ripeti = String.valueOf(pswf.getRipetipsw().getPassword());
					FileMan fout = new FileMan(UUID.randomUUID() + ".txt");
					fout.openOut(false);
					segrFile.openIn();
					Segreteria s; boolean found = false;
					while((s = RWDomain.readSegreteria(segrFile)) != null) {
						if (!found && temp.getCredenziali().getEmail().equals(s.getCredenziali().getEmail())) {
							found = true;
							if(vecchia.equals("") || nuova.equals("")) {
								JOptionPane.showMessageDialog(null,"Almeno uno dei campi è vuoto, modifica password annullato.");
								return;
							}
							if(!vecchia.equals(s.getCredenziali().getPassword())){
								JOptionPane.showMessageDialog(null,"La vecchia password è errata, modifica password annullato.");
								return;
							}
							if(!nuova.equals(ripeti)){
								JOptionPane.showMessageDialog(null,"Le due nuove password non corrispondono, modifca password annullato.");
								return;
							}
							if(vecchia.equals(nuova)){
								JOptionPane.showMessageDialog(null,"La vecchia e la nuova password sono identiche, modifca password annullato.");
								return;
							}
							s.setCredenziali(s.getCredenziali().getEmail(), nuova);
							JOptionPane.showMessageDialog(null,"Password modificata correttamente.");
						}
						RWDomain.writeSegreteria(fout, s);
					}
					fout.close(); segrFile.close();
					segrFile.getFile().delete();
					File f1 = new File(segrFile.getNome());
					fout.getFile().renameTo(f1);
					segrFile.resetFile(segrFile.getNome());

				} catch (IOException e) {
					JOptionPane.showMessageDialog(null,"Errore nell'apertura del file.");
				}
				break;
			case 2:
				rimuoviSegreteria(temp.getCredenziali().getEmail());
				JOptionPane.showMessageDialog(null,"Rimozione effettuata.");
				break;
			default:
				JOptionPane.showMessageDialog(null,"Scelta errata");
				break;
		}
	}

	public boolean modificaSegreteria(String email){
		boolean found = false; Segreteria temp, segreteria = null;
		try {
			Main.segrFile.openIn();
			while ((temp = RWDomain.readSegreteria(Main.segrFile)) != null) {
				if (temp.getCredenziali().getEmail().equals(email)) {
					found = true;
					segreteria = temp;
					break;
				}
			}
			if(!found)
				JOptionPane.showMessageDialog(null,"Non è stato trovato alcuna segreteria corrispondente all'email inserita.");
			else{
				ModificaSegreteria ms = new ModificaSegreteria(segreteria);ms.setVisible(true);
			}

			menuAdmin();
			return found;
		} catch (IOException e) {
			return false;
		}
	}
}