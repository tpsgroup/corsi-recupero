package domain.users;

import domain.*;
import enums.Anno;
import enums.GiornoSettimana;
import enums.Materia;
import main.Main;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utils.FileMan;
import utils.Inputs;
import utils.RWDomain;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;

import static java.lang.Integer.parseInt;

public class Segreteria extends Utente {

	public Segreteria(String nome, String cognome, Credenziali credenziali) {
		super(Type.SEGRETERIA, nome, cognome, credenziali);
	}

	public Corso creaCorso(FileMan f, ArrayList<Corsista> alunni, Materia materia, Anno anno) {
		Corso c = null;
		try {
			c = new Corso(alunni, materia, anno);
			if(!RWDomain.writeCorso(f, c, false))
				throw new IOException();
			//System.out.println("--- Corso registrato correttamente ---");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Errore nella scrittura nel file. Registrazione annullata.");
		}
		return c;
    }

	public String toString() {
		return "Segreteria: " +  this.getCognome() + " " + this.getNome() + "\n" +
				"Email: " + this.getCredenziali().getEmail();
	}

	public Calendario generaCalendario() {
		Calendario calendario = null;
		ArrayList<DocenteAssegnato> docenti = new ArrayList<>();
		GregorianCalendar[] dateArrey = new GregorianCalendar[Giorno.NUMCORSI * Calendario.NUMLEZIONI];
		GregorianCalendar inizio;
		GregorianCalendar inizioApp;
		ArrayList<Corso> corsiAssegnati = new ArrayList<>(); byte numCorsi;
        byte numIncontri=0;
		boolean done = false;
		GregorianCalendar date; int z=0;
		try {
			Docente docTemp; Corso corsoTemp;
			Main.docentiFile.openIn();
			while((docTemp = RWDomain.readDocente(Main.docentiFile)) != null)
				docenti.add(new DocenteAssegnato(docTemp));
			Main.docentiFile.close();
			Main.corsiFile.openIn();
			while((corsoTemp = RWDomain.readCorso(Main.corsiFile, false)) != null)
				corsiAssegnati.add(corsoTemp);
			Main.corsiFile.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Generazione Calendario: errore nella lettura dei file.");
			return null;
		}
		short[] corsiPerAnno = new short[Giorno.NUMCORSI];
		for(Corso corso : corsiAssegnati)
			++corsiPerAnno[corso.getAnno().ordinal()];
		{ // per dichiarare variabili da utilizzare solo in questo momento e poi dimenticare
			boolean areTooMany = false;
			String tooMany = "Generazione Calendario: troppi corsi per ogni anno.\n" +
					"Assicurarsi che il numero di corsi per ogni anno sia al massimo di " + Calendario.NUMGIORNI + ".\n" +
					"Anni con numero eccessivo di corsi-> ";
			for(byte i=0; i < Giorno.NUMCORSI; ++i){
				if(corsiPerAnno[i] <= Calendario.NUMGIORNI)
					continue;
				areTooMany = true;
				tooMany += "|" + Anno.values()[i].name() + "| ";
			}
			if(areTooMany){
				JOptionPane.showMessageDialog(null,tooMany);
				return null;
			}
		}
		for(Corso corsoTemp : corsiAssegnati) {
			boolean assegnato = false;
			for(DocenteAssegnato da : docenti){
				if(da.getnCorsi() == 0 || !da.getAnnoCorso()[corsoTemp.getAnno().ordinal()])
					continue;
				for(Materia m : da.getMaterie()){
					if(m.ordinal() == corsoTemp.getMateria().ordinal()) {
						assegnato = true;
						da.setnCorsi((byte) (da.getnCorsi() - 1));
						break;
					}
				}
				if(assegnato){
					corsoTemp.setDocente(da);
					break;
				}
			}
			if(corsoTemp.getDocente() == null){
				JOptionPane.showMessageDialog(null,"Genera Calendario: non ci " +
						"sono abbastanza docenti per i corsi inseriti.\n" +
						"ID corso senza docente: " + corsoTemp.getID());
				return null;
			}
		}

		numCorsi = (byte) corsiAssegnati.size();
		for(int start = 0; start < numCorsi && !done; ++start){
			calendario = new Calendario();
			done = true;
			for(Corso c : corsiAssegnati){
				c.getDocente().setGiorniOccupati(new boolean[Calendario.NUMGIORNI]);
				c.getDocente().getGiorniOccupati()[ c.getDocente().getFreeDay().ordinal()] = true;
			}
			Giorno[] g = calendario.getGiorni();
			Corso c= corsiAssegnati.get(start);
			for(int k=0; k < numCorsi;
                ++k, c = corsiAssegnati.get((start+k) % numCorsi)){
				boolean found = false;
				for(int i=0; i < Calendario.NUMGIORNI; ++i){
					Corso[] corsiGiorno = g[i].getCorsi();
					if(c.getDocente().getGiorniOccupati()[i] || 
							corsiGiorno[c.getAnno().ordinal()] != null)
						continue;
					corsiGiorno[c.getAnno().ordinal()] = c;
					c.getDocente().getGiorniOccupati()[i] = true;
					c.setOrario(g[i].getGiorno());
					found = true;
					break;
				}
				if(!found){
					done = false;
					break;
				}
			}
		}

		if(!done){
			JOptionPane.showMessageDialog(null,"Genera Calendario: non è possibile collocare i corsi.");
			return null;
		}
		int n=0;
		do{
			 n = JOptionPane.showConfirmDialog(null,
					"Vuoi inserire un'altra data in cui non fare i corsi?","data",JOptionPane.YES_NO_OPTION);
			if(n==0) {
				Inputs.contt=0;
				String dateappp=JOptionPane.showInputDialog(null,"Inserisci la data","data",JOptionPane.OK_OPTION);
				date=Inputs.stringToGregCal(dateappp);
				dateArrey[z++]=date;
			}
		}while(n==0);
		int numIncInt=0;
		do{
			String numIncStr = JOptionPane.showInputDialog(null,"Quanti incontri vuoi fare per ogni corso? [min. " + Calendario.NUMLEZIONI + "]: ","incontri",JOptionPane.OK_OPTION);
			try {
				numIncInt = parseInt(numIncStr);
				numIncontri = (byte) numIncInt;
				if(numIncontri < Calendario.NUMLEZIONI)
					JOptionPane.showMessageDialog(null,"Numero minimo di lezioni: " + Calendario.NUMLEZIONI + ".");
			}catch (Exception e)
			{
				JOptionPane.showMessageDialog(null, "Inserire un numero intero","ERRORE", JOptionPane.ERROR_MESSAGE);
			}
		}while(numIncontri < Calendario.NUMLEZIONI);
        boolean c;
        do {
            c=false;
			Inputs.contt=0;
            String inizioS = JOptionPane.showInputDialog(null,"Inserisci la data da cui iniziare i corsi (data di un lunedì): ","data",JOptionPane.OK_OPTION);
            inizio = Inputs.getDate(inizioS);
            int giornod = inizio.get(Calendar.DAY_OF_WEEK);
            if (giornod != Calendar.MONDAY) {
				JOptionPane.showMessageDialog(null,"Inserire una data che sia lunedì.","ATTENZIONE",JOptionPane.WARNING_MESSAGE);
                c = true;
            }
        }while(c);

		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Calendario");
			Row firstRow = sheet.createRow(0);
			firstRow.createCell(0).setCellValue("N°");
			firstRow.createCell(1).setCellValue("ID");
			firstRow.createCell(2).setCellValue("DISCIPLINA");
			firstRow.createCell(3).setCellValue("DOCENTI");
			firstRow.createCell(4).setCellValue("N° ALUNNI");
			firstRow.createCell(5).setCellValue("ANNO");
			firstRow.createCell(6).setCellValue("CLASSI");
			firstRow.createCell(7).setCellValue("GIORNO");
			for(int i=0; i < numIncontri; ++i){
				firstRow.createCell(8 + (i*2)).setCellValue("LEZIONE " + (i+1));
				firstRow.createCell(9 + (i*2));
			}

			inizioApp= inizio;
			for(int giorni = 0, numCorso=1; giorni < Calendario.NUMGIORNI; giorni++, inizioApp.add(Calendar.DATE, 1)) {
				for(int corsi=0;corsi<Giorno.NUMCORSI;corsi++) {
					Corso corso = calendario.getGiorni()[giorni].getCorsi()[corsi];
					//stampa "inizio" per la prima colonna incrementando il giono da 0 a 6
					if(corso == null)
						continue;
					inizio= (GregorianCalendar) inizioApp.clone();
					Row row = sheet.createRow(numCorso);
					row.createCell(0).setCellValue(numCorso);
					row.createCell(1).setCellValue(corso.getID());
					row.createCell(2).setCellValue(corso.getMateria().name());
					row.createCell(3).setCellValue(corso.getDocente().getCognome());
					row.createCell(4).setCellValue(corso.getAlunni().size());
					row.createCell(5).setCellValue("" + (corso.getAnno().ordinal() + 1));
					row.createCell(6).setCellValue(corso.classiCoinvolte());
					row.createCell(7).setCellValue(GiornoSettimana.values()[giorni].name().toLowerCase());
					for(int datee=0; datee<numIncontri; datee++) {
						for(int festa=0; festa < z ; festa++) {
							if(dateArrey[festa].compareTo(inizio) == 0) {
								inizio.add(Calendar.DATE, 7);
							}
						}
						row.createCell(8 + (datee*2)).setCellValue(Inputs.gregCalToString(inizio));
						row.createCell(9 + (datee*2)).setCellValue(corso.getOrario());
						inizio.add(Calendar.DATE, 7);
					}
					++numCorso;
				}
			}
			try (FileOutputStream outputStream = new FileOutputStream("Calendario.xlsx")) {
				workbook.write(outputStream);
			}
			workbook.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Errore nella scrittura di 'Calendario.xlsx'.");
			return null;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Errore generico nella scrittura del file.","ERRORE",JOptionPane.ERROR_MESSAGE);
			return null;
		}
		return calendario;
	}

	public void generaCorsi(ArrayList<Corsista> corsisti) {
		ArrayList<Corsista>[][] corsisti_materie = new ArrayList[Anno.values().length][Materia.values().length];
		String[] id = new String[50]; String fine = "\n";
		int l=0;

		for(int k = 0; k < Anno.values().length; ++k) {
			for (int i = 0; i < Materia.values().length; i++) {
				corsisti_materie[k][i] = new ArrayList<>();
			}
		}

		for(Corsista c : corsisti) {
			for(Materia m : c.getMaterie()) {
				if(m != null)
					corsisti_materie[c.getAnno().ordinal()][m.ordinal()].add(c);
			}
		}

		for(int i = 0; i < Anno.values().length; ++i) {
			for(int k = 0; k < Materia.values().length; ++k)
				corsisti_materie[i][k].sort(Comparator.comparing(Corsista::getSezione));
		}
		try {
			Main.corsiFile.openOut(false);

			for(int i = 0; i < Anno.values().length; ++i) {
				for(int k = 0; k < Materia.values().length; ++k) {
					int ncorsisti = corsisti_materie[i][k].size();
					if(ncorsisti >= Corso.MIN_ALUNNI && ncorsisti <= 4 * Corso.MAX_ALUNNI) {
						byte ncorsi;
						ncorsi = (byte) (ncorsisti/Corso.MAX_ALUNNI);
						while(ncorsisti - (ncorsi * Corso.MAX_ALUNNI) >= Corso.MIN_ALUNNI) {
							ncorsi++;
						}
						int nultimocorso = 0;
						if(ncorsi > 1) {
							nultimocorso = (ncorsisti%Corso.MAX_ALUNNI);
//							nultimocorso = (ncorsi - 1) * Corso.MAX_ALUNNI - ncorsisti%Corso.MAX_ALUNNI;
							if(nultimocorso < Corso.MIN_ALUNNI)
								nultimocorso = Corso.MAX_ALUNNI;
						} else if(ncorsisti < Corso.MAX_ALUNNI)
							nultimocorso = ncorsisti;
						else
							nultimocorso = Corso.MAX_ALUNNI;



						for(int h = 0; h < ncorsi; ++h) {
							ArrayList<Corsista> alunni = new ArrayList<>();
							for(int j = h*Corso.MAX_ALUNNI; j < (Corso.MAX_ALUNNI*(h+1)) ; ++j) {
								if(h+1 == ncorsi) {
									for(int o = j; o < (nultimocorso + h*Corso.MAX_ALUNNI) && o < ncorsisti; ++o) {
										alunni.add(corsisti_materie[i][k].get(o));
									}
									break;
								}
								alunni.add(corsisti_materie[i][k].get(j));
							}
							Corso c = this.creaCorso(Main.corsiFile, alunni, Materia.values()[k], Anno.values()[i]);
							id[l]="ID del corso (" + Materia.values()[k] + " - " + Anno.values()[i] + " anno): " + c.getID() + ".";
							l++;
						}
					} else if(ncorsisti > 4 * Corso.MAX_ALUNNI) {
						JOptionPane.showMessageDialog(null,"Non è possibile generare i corsi di " + Materia.values()[k] + " - " + Anno.values()[i] + " anno, sono più di " + 4 * Corso.MAX_ALUNNI);
					}
				}
			}
			for(l=0;l<50;l++)
			{
				if(id[l]!=null)
				{
					fine = fine + id[l]+"\n";
				}
			}
			JOptionPane.showMessageDialog(null,fine,"Corsi",JOptionPane.OK_OPTION);

			Main.corsiFile.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Non è possibile aprire il file. Generazione corsi annullata.");
		}
	}

    public void visualizzaCorso(int id) {
		Corso corso; boolean found;
		String stampa; String fine;
		try {
			found = false;
			Main.corsiFile.openIn();
			while((corso = RWDomain.readCorso(Main.corsiFile, false)) != null){
				if(corso.getID() != id)
					continue;
				stampa=corso.corsistiToString();
				fine=corso + "Lista corsisti:\n"+stampa;
				JOptionPane.showMessageDialog(null,fine,"Corso con ID: "+id,JOptionPane.INFORMATION_MESSAGE);
				found = true;
			}

			if(!found)
				JOptionPane.showMessageDialog(null,"Nessun corso trovato.");
			Main.corsiFile.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Visualizza corso: errore nell'apertura del file.");
		}
	}
}