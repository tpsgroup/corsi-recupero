package domain.users;

import domain.Credenziali;
import enums.GiornoSettimana;
import enums.Materia;

import java.util.ArrayList;

public class Docente extends Utente {
	protected ArrayList<Materia> materie;
	protected boolean[] annoCorso; //4 anni, uno per ogni anno in cui può insegnare
	protected GiornoSettimana freeDay;
	protected byte nCorsi;
	protected boolean sestaLun;

	public Docente(String nome, String cognome, Credenziali credenziali, ArrayList<Materia> materie,
				   boolean[] annoCorso, GiornoSettimana freeDay, byte nCorsi, boolean sestaLun) {
		super(Type.DOCENTE, nome, cognome, credenziali);
		this.materie = materie;
		this.annoCorso = annoCorso;
		this.freeDay = freeDay;
		this.nCorsi = nCorsi;
		this.sestaLun = sestaLun;
	}

    public Docente() {  }

	public ArrayList<Materia> getMaterie() {
		return materie;
	}

	public void setMaterie(ArrayList<Materia> materie) {
		this.materie = materie;
	}

	public boolean[] getAnnoCorso() {
		return annoCorso;
	}

	public void setAnnoCorso(boolean[] annoCorso) {
		this.annoCorso = annoCorso;
	}

	public GiornoSettimana getFreeDay() {
		return freeDay;
	}

	public void setFreeDay(GiornoSettimana freeDay) {
		this.freeDay = freeDay;
	}

	public byte getnCorsi() {
		return nCorsi;
	}

	public void setnCorsi(byte nCorsi) {
		this.nCorsi = nCorsi;
	}

	public boolean getSestaLun() {
		return sestaLun;
	}

	public void setSestaLun(boolean sestaLun) {
		this.sestaLun = sestaLun;
	}

	public String toString() {
		String str = "Docente: " +  this.getCognome() + " " + this.getNome() + "\n" +
				"Email: " + this.getCredenziali().getEmail() + "\n" +
				//se true, ha richiesto insegnamento o per il primo o per il secondo anno
				//se false, ha automaticamente espresso una preferenza per il terzo o quarto anno
				((this.annoCorso[0]) ? "Ha espresso preferenza nell'insegnare nel biennio.\n" :
				"Ha espresso preferenza nell'insegnare nel triennio.\n") +
				"Num. di corsi per cui si rende disponibile: " + this.getnCorsi() + "\n" +
				"Materie: ";
		for(Materia m : materie)
			str += (m.name().toLowerCase() + ", ");

		str = str.substring(0, str.length() -2); //toglie gli ultimi due ", "
		str += "\nGiorno libero: " + this.getFreeDay().name().toLowerCase() + "\n";
		str += (this.getSestaLun()) ? "Il docente ha la sesta ora il lunedì.\n" :
				"Il docente non ha la sesta ora il lunedì.\n";
		return str;
	}
}