package domain.users;

import domain.Credenziali;
import main.Main;
import utils.Inputs;

import javax.swing.*;
import java.io.IOException;

import static utils.Utils.trovaDocenteByEmail;
import static utils.Utils.trovaSegreteriaByEmail;

public class Utente {
	public Type getTipo() {
		return tipo;
	}

	public enum Type {
		ADMIN, SEGRETERIA, DOCENTE, CORSISTA
	}
	private Type tipo;
	private String nome;
	private String cognome;
	private Credenziali credenziali;

	public Utente(Type tipo, String nome, String cognome, Credenziali credenziali) {
		this.tipo = tipo;
		this.nome = nome;
		this.cognome = cognome;
		this.credenziali = credenziali;
	}

    public Utente() {
        nome = cognome = null; tipo = null;
        credenziali = new Credenziali();
    }

    public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Credenziali getCredenziali() {
		return credenziali;
	}

	public void setCredenziali(String email, String password) {
		this.credenziali.setEmail(email);
		this.credenziali.setPassword(password);
	}
}