package domain;

import domain.users.DocenteAssegnato;
import enums.Anno;
import enums.GiornoSettimana;
import enums.Materia;
import enums.Sezione;

import java.util.ArrayList;

public class Corso {
	public static final byte MIN_ALUNNI = 15, MAX_ALUNNI = 30;
	private ArrayList<Corsista> alunni;
	private Materia materia;
	private DocenteAssegnato docente;
	private Anno anno;
	private String orario;
	private final int ID;

	public Corso(ArrayList<Corsista> alunni, Materia materia, DocenteAssegnato docente, Anno anno) {
		this.alunni = alunni;
		this.materia = materia;
		this.docente = docente;
		this.anno = anno;
		this.orario = "";
		this.ID = (int) (Math.random() * 1000) + 1;
	}

	public Corso(ArrayList<Corsista> alunni, Materia materia, Anno anno) {
		this.alunni = alunni;
		this.materia = materia;
		this.docente = null;
		this.anno = anno;
		this.orario = "";
		this.ID = (int) (Math.random() * 1000) + 1;
	}

	public Corso(int ID, ArrayList<Corsista> alunni, Materia materia, Anno anno) {
		this.alunni = alunni;
		this.materia = materia;
		this.docente = null;
		this.anno = anno;
		this.orario = "";
		this.ID = ID;
	}

    public Corso(int ID, ArrayList<Corsista> alunni, Materia materia, DocenteAssegnato docente, Anno anno) {
		this.alunni = alunni;
		this.materia = materia;
		this.docente = docente;
		this.anno = anno;
		this.orario = "";
		this.ID = ID;
    }

    public ArrayList<Corsista> getAlunni() {
		return alunni;
	}

	public void setAlunni(ArrayList<Corsista> alunni) {
		this.alunni = alunni;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public DocenteAssegnato getDocente() {
		return docente;
	}

	public void setDocente(DocenteAssegnato docente) {
		this.docente = docente;
	}

	public Anno getAnno() {
		return anno;
	}

	public void setAnno(Anno anno) {
		this.anno = anno;
	}

	public String getOrario(){
		return this.orario;
	}

	public void setOrario(GiornoSettimana giorno) {
		if(giorno == GiornoSettimana.MARTEDI || giorno == GiornoSettimana.GIOVEDI ||
				(anno == Anno.PRIMO && giorno == GiornoSettimana.LUNEDI))
			orario = Giorno.ORARIO_SESTA;
		else
			orario = Giorno.ORARIO_QUINTA;
	}

	public String toString() {
		String str = this.materia.name() + "-"+ (this.anno.ordinal() + 1) + " (ID: " + this.ID + ")";
		if(!this.orario.equals(""))
			str += " [" + this.orario + "]";
		str += "\nNumero di corsisti: " + this.alunni.size() + "\n" +
				"Classi coinvolte: " + this.classiCoinvolte() + "\n";
		if(docente != null)
			str += "Docente: " + this.docente.getCognome() + " " + this.docente.getNome();
		return str;
	}

	public String classiCoinvolte() {
		Sezione[] sezioni = Sezione.values(); byte numSez = (byte) sezioni.length;
		boolean[] classi = new boolean[numSez];
		String str = "";
		for(Corsista c : alunni) {
			classi[ c.getSezione().ordinal()] = true;
		}
		for(int i=0; i < numSez; ++i) {
			if(classi[i])
				str += (sezioni[i] + ", ");
		}
		return str.substring(0, str.length()-2);
	}

	public String corsistiToString(){
		String str = "";
		for(Corsista a : this.alunni){
			str += a.toString() + "\n";
		}
		return str;
	}

	public int getID() {
		return ID;
	}
}