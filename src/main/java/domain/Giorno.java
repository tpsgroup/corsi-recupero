package domain;

import enums.GiornoSettimana;

public class Giorno {
	public static final byte NUMCORSI = 4;
	private Corso[] corsi;
	private GiornoSettimana giorno;
	public static final String ORARIO_QUINTA = "13:30 - 15:30", ORARIO_SESTA = "14:30 - 16:30";

	public Giorno(GiornoSettimana giorno) {
		this.giorno = giorno;
		this.corsi = new Corso[NUMCORSI];
	}

	public Corso[] getCorsi() {
		return corsi;
	}

	public void setCorsi(Corso[] corsi) {
		this.corsi = corsi;
		for(int i=0; i < NUMCORSI; ++i)
			this.corsi[i].setOrario(giorno);
	}

	public GiornoSettimana getGiorno() {
		return giorno;
	}

	public void setGiorno(GiornoSettimana giorno) {
		this.giorno = giorno;
	}
}