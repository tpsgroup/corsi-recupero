package utils;

import java.io.File;
import java.util.ArrayList;
import javax.swing.filechooser.FileFilter;

public class FileTypeFilter extends FileFilter {
    private ArrayList<String> extensions;
    private String description;

    public FileTypeFilter(ArrayList<String> extensions, String description) {
        this.extensions = extensions;
        this.description = description;
    }

    public boolean accept(File file) {
        if (file.isDirectory()) {
            return true;
        }
        for(String e : extensions) {
            if(file.getName().endsWith(e))
                return true;
        }
        return false;
    }

    public String getDescription() {
        return description + String.format(" (*%s)", extensions);
    }
}