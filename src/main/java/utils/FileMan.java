package utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

// Classe per la gestione di file ad accesso sequenziale
public class FileMan {
    enum Format {
        BOOL, BYTE, CHAR, DOUBLE, FLOAT,
        INT, LONG, SHORT, UTF
    };
    private String nome;
    private File file;
    private FileInputStream fileIn;
    private BufferedInputStream bufIn;
    private DataInputStream dataIn;
    private FileOutputStream fileOut;
    private BufferedOutputStream bufOut;
    private DataOutputStream dataOut;

    public FileMan(String nome) {
        this.nome = nome;
        this.file = new File(nome);
    }

    public void resetFile(String nome) {
        this.nome = nome;
        this.file = new File(nome);
    }

    public String getNome() {
        return nome;
    }

    public void openOut(boolean append) throws IOException {
        try {
            fileOut = new FileOutputStream(file, append);
            bufOut = new BufferedOutputStream(fileOut);
            dataOut = new DataOutputStream(bufOut);
            System.gc(); //garbage collector
        } catch (Exception e) {
            throw new IOException();
        }
    }

    public void closeIn() {
        try {
            dataIn.close(); bufIn.close(); fileIn.close();
        } catch (Exception ignored) {} //già chiuso
    }

    public void openIn() throws IOException {
        try {
            fileIn = new FileInputStream(file);
            bufIn = new BufferedInputStream(fileIn);
            dataIn = new DataInputStream(bufIn);
            System.gc(); //garbage collector
        } catch (Exception e) {
            throw new IOException();
        }
    }

    public void closeOut() {
        try {
            dataOut.flush(); bufOut.flush(); fileOut.flush();
            dataOut.close(); bufOut.close(); fileOut.close();
        } catch (Exception ignored) {} //già chiuso
    }

    public void close() {
        closeIn(); closeOut();
    }

    public Object read(Format format) throws IOException, EOFException {
        try {
            switch (format) {
                case BOOL:
                    return dataIn.readBoolean();
                case BYTE:
                    return dataIn.readByte();
                case CHAR:
                    return dataIn.readChar();
                case DOUBLE:
                    return dataIn.readDouble();
                case FLOAT:
                    return dataIn.readFloat();
                case INT:
                    return dataIn.readInt();
                case LONG:
                    return dataIn.readLong();
                case SHORT:
                    return dataIn.readShort();
                case UTF:
                    return dataIn.readUTF();
                default: //nessun formato corrispondente
                    throw new IOException();
            }
        } catch (EOFException eof) {
            throw new EOFException();
        } catch (Exception e) { throw new IOException(); }
    }

    public void write(Format format, Object data) throws IOException {
        try {
            switch (format) {
                case BOOL:
                    dataOut.writeBoolean((boolean) data);
                    break;
                case BYTE:
                    dataOut.writeByte((byte) data);
                    break;
                case CHAR:
                    dataOut.writeChar((char) data);
                    break;
                case DOUBLE:
                    dataOut.writeDouble((double) data);
                    break;
                case FLOAT:
                    dataOut.writeFloat((float) data);
                    break;
                case INT:
                    dataOut.writeInt((int) data);
                    break;
                case LONG:
                    dataOut.writeLong((long) data);
                    break;
                case SHORT:
                    dataOut.writeShort((short) data);
                    break;
                case UTF:
                    dataOut.writeUTF((String) data);
                    break;
                default: //nessun formato corrispondente
                    throw new IOException();
            }
        } catch (EOFException eof) {
            throw new EOFException();
        } catch (Exception e) { throw new IOException(); }

    }

    public File getFile() {
        return this.file;
    }
}
