package utils;

import domain.*;
import domain.users.Admin;
import domain.users.Docente;
import domain.users.DocenteAssegnato;
import domain.users.Segreteria;
import enums.Anno;
import enums.GiornoSettimana;
import enums.Materia;
import enums.Sezione;

import javax.swing.*;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;

/** Classe per separare le operazioni di lettura e
 *  scrittura da file dal resto del programma */
public class RWDomain {
    /** writeXXX e readXXX devono essere usati aprendo e chiudendo
     *  il utils.FileMan prima e dopo le operazioni in Main*/

    public static boolean writeSegreteria(FileMan f, Segreteria s) {
        try {
            f.write(FileMan.Format.UTF, s.getNome());
            f.write(FileMan.Format.UTF, s.getCognome());
            f.write(FileMan.Format.UTF, s.getCredenziali().getEmail());
            f.write(FileMan.Format.UTF, s.getCredenziali().getPassword());
              
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static Segreteria readSegreteria(FileMan f) {
        String nome, cognome, email, password;
        try {
            nome = (String) f.read(FileMan.Format.UTF);
            cognome = (String) f.read(FileMan.Format.UTF);
            email = (String) f.read(FileMan.Format.UTF);
            password = (String) f.read(FileMan.Format.UTF);

            return new Segreteria(nome, cognome,
                    new Credenziali(email, password));
        } catch (EOFException eof) { //nessun altro record
            return null;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Errore durante la lettura del file.");
            return null;
        }
    }

    public static void writeAdmin(FileMan f, Admin a) {
        try {
            f.write(FileMan.Format.UTF, a.getNome());
            f.write(FileMan.Format.UTF, a.getCognome());
            f.write(FileMan.Format.UTF, a.getCredenziali().getEmail());
            f.write(FileMan.Format.UTF, a.getCredenziali().getPassword());

        } catch (IOException ignored) { }
    }

    public static Admin readAdmin(FileMan f) {
        String nome, cognome, email, password;
        try {
            nome = (String) f.read(FileMan.Format.UTF);
            cognome = (String) f.read(FileMan.Format.UTF);
            email = (String) f.read(FileMan.Format.UTF);
            password = (String) f.read(FileMan.Format.UTF);

            return new Admin(nome, cognome,
                    new Credenziali(email, password));
        } catch (EOFException eof) { //nessun altro record
            return null;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Errore durante la lettura del file.");
            return null;
        }
    }

    public static boolean writeDocente(FileMan f, Docente d) {
        try {
            f.write(FileMan.Format.UTF, d.getNome());
            f.write(FileMan.Format.UTF, d.getCognome());
            f.write(FileMan.Format.UTF, d.getCredenziali().getEmail());
            f.write(FileMan.Format.UTF, d.getCredenziali().getPassword());
            // prima di scrivere la lista di materie del docente,
            // indico di quanti elementi è composta la lista
            // per facilitarne la lettura
            f.write(FileMan.Format.BYTE, (byte) d.getMaterie().size());
            for(Materia m : d.getMaterie())
                f.write(FileMan.Format.BYTE, (byte) m.ordinal());
            for(byte i = 0; i < Giorno.NUMCORSI; ++i)
                f.write(FileMan.Format.BOOL, d.getAnnoCorso()[i]);
            f.write(FileMan.Format.BYTE, (byte) d.getFreeDay().ordinal());
            f.write(FileMan.Format.BYTE, d.getnCorsi());
            f.write(FileMan.Format.BOOL, d.getSestaLun());
              
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static Docente readDocente(FileMan f) {
        String nome, cognome, email, password;
        ArrayList<Materia> materie = new ArrayList<>(); byte numMaterie;
        boolean[] annoCorso = new boolean[Giorno.NUMCORSI];
        GiornoSettimana freeDay; byte nCorsi; boolean sestaLun;

        try {
            nome = (String) f.read(FileMan.Format.UTF);
            cognome = (String) f.read(FileMan.Format.UTF);
            email = (String) f.read(FileMan.Format.UTF);
            password = (String) f.read(FileMan.Format.UTF);
            numMaterie = (byte) f.read(FileMan.Format.BYTE);
            for(byte i=0; i < numMaterie; ++i)
                materie.add(readMateria(f));
            for(byte i = 0; i < Giorno.NUMCORSI; ++i)
                annoCorso[i] = (boolean) f.read(FileMan.Format.BOOL);
            freeDay = readGiornoSettimana(f);
            nCorsi = (byte) f.read(FileMan.Format.BYTE);
            sestaLun = (boolean) f.read(FileMan.Format.BOOL);

            return new Docente(nome, cognome, new Credenziali(email, password),
                    materie, annoCorso, freeDay, nCorsi, sestaLun);
        } catch (EOFException eof) { //nessun altro record
            return null;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Errore durante la lettura del file.");
            return null;
        }
    }

    private static GiornoSettimana readGiornoSettimana(FileMan f) {
        try {
            byte index = (byte) f.read(FileMan.Format.BYTE);
            return GiornoSettimana.values()[index];
        } catch (IOException e) {
            return null;
        }
    }

    public static boolean writeCorso(FileMan f, Corso c, boolean docente) {
        try {
            f.write(FileMan.Format.INT, c.getID());
            f.write(FileMan.Format.BYTE, (byte) c.getMateria().ordinal());
            f.write(FileMan.Format.BYTE, (byte) c.getAnno().ordinal());
            f.write(FileMan.Format.BYTE, (byte) c.getAlunni().size());
            for(Corsista a : c.getAlunni()){
                f.write(FileMan.Format.UTF, a.getNome());
                f.write(FileMan.Format.UTF, a.getCognome());
                f.write(FileMan.Format.UTF, a.getCredenziali().getEmail());
                f.write(FileMan.Format.BYTE, (byte) a.getAnno().ordinal());
                f.write(FileMan.Format.BYTE, (byte) a.getSezione().ordinal());
                for(Materia m : a.getMaterie()) {
                    if(m == null)
                        f.write(FileMan.Format.BYTE, (byte) -1);
                    else
                        f.write(FileMan.Format.BYTE, (byte) m.ordinal());
                }
            }
            if(docente)
                if(!writeDocente(f, c.getDocente()))
                    throw new IOException();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static Corso readCorso(FileMan f, boolean docente) {
        int ID; Materia m; Anno a;
        ArrayList<Corsista> alunni = new ArrayList<>();
        try {
            ID = (int) f.read(FileMan.Format.INT);
            m = readMateria(f);
            a = readAnno(f);
            byte numAlunni = (byte) f.read(FileMan.Format.BYTE);
            for(int i=0; i < numAlunni; ++i){
                String n = (String) f.read(FileMan.Format.UTF);
                String c = (String) f.read(FileMan.Format.UTF);
                String e = (String) f.read(FileMan.Format.UTF);
                Anno anno = readAnno(f);
                Sezione s = readSezione(f);
                Materia[] materie = new Materia[Corsista.MAXCORSI];
                for(int j = 0; j < Corsista.MAXCORSI; ++j)
                    materie[j] = readMateria(f);
                alunni.add( new Corsista(n, c, e, s, anno, materie) );
            }
            if(docente){
                DocenteAssegnato da = new DocenteAssegnato(readDocente(f));
                return new Corso(ID, alunni, m, da, a);
            } else
                return new Corso(ID, alunni, m, a);
        } catch (EOFException eof) { //nessun altro record
            return null;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Errore durante la lettura del file.");
            return null;
        }
    }

    private static Sezione readSezione(FileMan f) {
        try {
            byte s = (byte) f.read(FileMan.Format.BYTE);
            return Sezione.values()[s];
        } catch (Exception e) {
            return null;
        }
    }

    private static Materia readMateria(FileMan f) {
        try {
            byte m = (byte) f.read(FileMan.Format.BYTE);
            return Materia.values()[m];
        } catch (Exception e) {
            return null;
        }
    }

    private static Anno readAnno(FileMan f) {
        try {
            byte a = (byte) f.read(FileMan.Format.BYTE);
            return Anno.values()[a];
        } catch (Exception e) {
            return null;
        }
    }

}
