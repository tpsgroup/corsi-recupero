package utils;

import domain.Corsista;
import domain.Credenziali;
import domain.users.Admin;
import domain.users.Docente;
import domain.users.Segreteria;
import domain.users.Utente;
import enums.Anno;
import enums.Materia;
import enums.Sezione;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import views.FileFrame;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static main.Main.*;

public class Utils {
    public final static String xlsx = "xlsx";
    public final static String xls = "xls";

    public static Utente loginWithCredentials(String email) {
        Utente u = null, temp;

        if ((temp = trovaAdminByEmail(email)) != null)
            u = temp;
        else if ((temp = trovaDocenteByEmail(email)) != null)
            u = temp;
        else if ((temp = trovaSegreteriaByEmail(email)) != null)
            u = temp;
        return u;
    }

    public static Segreteria trovaSegreteriaByEmail(String email) {
        boolean presente;
        try {
            segrFile.openIn();
            while(true)
            {
                try {
                    Segreteria s = RWDomain.readSegreteria(segrFile);
                    presente = email.equals(s.getCredenziali().getEmail());
                    if (presente) {
                        segrFile.close();
                        return s;
                    }
                } catch (Exception ex) {
                    segrFile.close();
                    return null;
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    public static Docente trovaDocenteByEmail(String email) {
        boolean presente;
        try {
            docentiFile.openIn();
            while(true)
            {
                try {
                    Docente d = RWDomain.readDocente(docentiFile);
                    presente = email.equals(d.getCredenziali().getEmail());
                    if (presente) {
                        docentiFile.close();
                        return d;
                    }
                } catch (Exception ex) {
                    docentiFile.close();
                    return null;
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    public static Admin trovaAdminByEmail(String email) {
        boolean presente;
        try {
            adminFile.openIn();
            while(true)
            {
                try {

                    Admin a = RWDomain.readAdmin(adminFile);
                    presente = email.equals(a.getCredenziali().getEmail());
                    if (presente) {
                        adminFile.close();
                        return a;
                    }
                } catch (Exception ex) {
                    adminFile.close();
                    return null;
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    public static ArrayList<Corsista> importDataFromExcel() {
        FileFrame frame = new FileFrame();
        File f = frame.getFD().getFile(); frame.setVisible(false);
        ArrayList<Corsista> alunni = new ArrayList<>();

        try {
            JOptionPane.showMessageDialog(null,"Operazione in corso... Premere ok e attendere...");
            Workbook workbook = WorkbookFactory.create(f);
            Sheet sheet = workbook.getSheetAt(0);
            sheet.removeRow(sheet.getRow(0));

            for (Row row : sheet) {
                try {
                    String nome, cognome, email; Sezione sez; Anno anno; Materia[] m = new Materia[Corsista.MAXCORSI];
                    nome = Optional.ofNullable(row.getCell(0))
                            .map(s -> !s.getStringCellValue().equals("") ? s.getStringCellValue().trim() : null)
                            .orElse(null);
                    if(nome == null)    // se la prima cella è vuota
                        break;          // interrompe l'inserimento
                    cognome = Optional.ofNullable(row.getCell(1))
                            .map(s -> !s.getStringCellValue().equals("") ? s.getStringCellValue().trim() : null)
                            .orElse(null);

                    try {
                        email = Optional.ofNullable(row.getCell(2))
                                .map(s -> !s.getStringCellValue().equals("") ? s.getStringCellValue().trim() : null)
                                .orElse(null);
                        if(Utils.trovaDocenteByEmail(email)!=null || Utils.trovaSegreteriaByEmail(email)!=null)
                            JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": utente già registrato.");
                        if(!Credenziali.isValid(email))
                            JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": inserire una email valida di dominio '@itisandria.it'");
                    } catch (NullPointerException npe) {
                        JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": errore nella cella 'Email'.");
                        continue;
                    }

                    try {
                        anno = Anno.values()[Optional.ofNullable(row.getCell(3))
                                .map(s -> (s.getNumericCellValue() >= 1 && s.getNumericCellValue() <= Anno.values().length)
                                        ? (int) s.getNumericCellValue()-1 : null)
                                .orElse(null)];
                    } catch (NullPointerException npe){
                        JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": errore nella cella 'Anno'.");
                        continue;
                    }
                    try {
                        sez = Sezione.valueOf(Optional.ofNullable(row.getCell(4))
                            .map(s -> !s.getStringCellValue().equals("") ? s.getStringCellValue().trim() : null)
                            .orElse(null));
                    } catch (NullPointerException npe){
                        JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": errore nella cella 'Sezione'.");
                        continue;
                    }

                    try{
                        for(int i=0; i < Corsista.MAXCORSI; ++i){
                            String str = Optional.ofNullable(row.getCell(5+i))
                                    .map(s -> !s.getStringCellValue().equals("") ?
                                            s.getStringCellValue().trim().toUpperCase() : null)
                                    .orElse(null);
                            if(str == null)
                                continue;
                            m[i] = Materia.valueOf(str);
                        }
                    } catch (NullPointerException npe){
                        JOptionPane.showMessageDialog(null,"Riga " + row.getRowNum() + ": errore nelle materie inserite.");
                        continue;
                    }

                    alunni.add(new Corsista(nome, cognome, email, sez, anno, m));
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null,"Errore generico nella riga " + row.getRowNum() + ", alunno non importato.");
                }
            }
            workbook.close();
            return alunni;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Errore nella lettura del file.");
            return null;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Errore generico nell'importazione.");
            return null;
        }
    }
}
