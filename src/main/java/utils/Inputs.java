package utils;

import javax.swing.*;
import java.util.GregorianCalendar;
import java.util.Scanner;

// "Libreria" per la gestione degli errori nell'input
//il parametro passato è il messaggio per l'input
public class Inputs {
	static final short SEC_IN_HR = 3600, SEC_IN_MIN = 60;
	public static int contt=0;
	static boolean errore; //variabile utilizzata per qualsiasi errore nel corso del programma
							//anche al di fuori della classe

	//Inserimento stringa
	public static String stringa(String mexIn) {
		Scanner input = new Scanner(System.in);
		String inserimento = null;
		do {
			JOptionPane.showMessageDialog(null,mexIn);
			errore = false;
			try {
				inserimento = input.nextLine();
				if(inserimento.length() == 0) //non è stato inserito nulla
					return null;
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Ritenta.");
				errore=true;
				input.reset();
			}	
		}while(errore);

		return inserimento != null ? inserimento.trim() : null;
	}

	//Inserimento numero intero
	public static long intero(String mexIn) {
		Scanner input = new Scanner(System.in);
		long inserimento = 0;
		do {
			errore = false;
			try {
				String num = stringa(mexIn);
				inserimento = Long.parseLong(num);
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Inserire un numero intero. Ritenta.");
				errore=true;
				input.reset();
			}
		}while(errore);
		return inserimento;
	}
	
	//Inserimento numero decimale
	public static double floating(String mexIn) {
		Scanner input = new Scanner(System.in);
		double inserimento = 0;
		do {
			errore = false;
			try {
				String num = stringa(mexIn);
				inserimento = Double.parseDouble(num);
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Inserire un numero decimale. Ritenta.");
				errore=true;
				input.reset();			
			}
		}while(errore);
		return inserimento;
	}
	
	//Inserimento data yyyy/mm/dd
	public static GregorianCalendar getDate(String date) {
		Scanner input = new Scanner(System.in);

		GregorianCalendar data = null;
		do {
			errore = false;
			try {
				data = stringToGregCal(date);
				if(data == null)
					throw new Exception();
			} catch(Exception e) { //non è stato inserito un numero per ogni campo
				contt++;
				if(contt<2) {
					JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Inserire la data nel seguente modo:\nGiorno(Max 2 cifre)/Mese(Max 2 cifre)/Anno(4 cifre)  Es: 25/12/2002");
					errore = true;
				}
				//input.reset();
			}
		}while(errore);
		return data;
	}
	public static GregorianCalendar stringToGregCal(String str) {
		int anno, mese, giorno;
		try {
			int indice = str.indexOf('/');
			String info = str.substring(0, indice); //prendo la stringa dell'anno
			giorno = Integer.parseInt(info);
			if(giorno < 1 || giorno > 31) //sono validi solo indice numeri da 1 a 31, al di l� del mese inserito
				throw new Exception();

			str = str.substring(indice+1).trim();
			indice = str.indexOf('/');

			info = str.substring(0, indice); //prendo la stringa del mese
			mese = Integer.parseInt(info);
			if(mese < 1 || mese > 12) //sono validi solo indice numeri da 1 a 12
				throw new Exception();

			str = str.substring(indice+1).trim();
			if(str.length() != 4) //valuta come errore l'inserimento di un anno che non abbia 4 cifre
				throw new Exception();
			anno = Integer.parseInt(str);

			return new GregorianCalendar(anno, mese-1, giorno);
		}catch (Exception e) {
			return null;
		}

	}

	public static char getChar(String mexIn) {
		Scanner input = new Scanner(System.in);
		String inserimento;
		char c = '\0';

		do {
			errore = false;
			try {
				inserimento = stringa(mexIn);
				c = inserimento.trim().charAt(0);

			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Ritenta.");
				errore=true;
				input.reset();
			}
		}while(errore);

		JOptionPane.showMessageDialog(null,"Viene considerato il carattere '" + c + "'...");
		return c;
	}

	//Inserimento tempo h:m:s
	public static long getTime(String mexIn) {
		Scanner input = new Scanner(System.in);
		long tempo = 0;
		int ore, minuti, secondi;
		do {
			errore = false;
			try {
				String str = stringa(mexIn);

				int indice = str.indexOf(':');
				String info = str.substring(0, indice); //prendo la stringa delle ore
				ore = Integer.parseInt(info);
				if(ore < 0 || ore > 24) //sono validi solo numeri da 0 a 24 (ore)
					throw new Exception();

				str = str.substring(indice+1).trim();
				indice = str.indexOf(':');
				info = str.substring(0, indice); //prendo la stringa del minuti
				minuti = Integer.parseInt(info);
				if(minuti < 0 || minuti > 59) //sono validi solo indice numeri da 1 a 12
					throw new Exception();

				str = str.substring(indice+1).trim();
				secondi = Integer.parseInt(str);
				if(secondi < 0 || secondi > 59) //sono validi solo indice numeri da 1 a 31, al di l� del minuti inserito
					throw new Exception();

				tempo = ore*SEC_IN_HR + minuti*SEC_IN_MIN + secondi;
			} catch(Exception e) { //non � stato inserito un numero per ogni campo

				JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Inserire il tempo nel seguente modo:ore:minuti:secondi  Es: 4:54:25");
				errore=true;
				input.reset();
			}
		}while(errore);
		return tempo;
	}

	public static String gregCalToString(GregorianCalendar g) {
		return g.get(GregorianCalendar.DATE) + "/" +
				(g.get(GregorianCalendar.MONTH) + 1) + "/" +
				g.get(GregorianCalendar.YEAR);
	}

	public static boolean bool(String mexIn) {
		Scanner input = new Scanner(System.in);
		String inserimento;
		char c = '\0';

		do {
			errore = false;
			try {
				inserimento = stringa(mexIn);
				c = inserimento.trim().toLowerCase().charAt(0);
				if(c != 's' && c != 'n')
					throw new Exception();
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Inserire solo 's' o 'n'.");
				errore=true;
				input.reset();
			}
		}while(errore);

		JOptionPane.showMessageDialog(null,"Viene considerato il carattere '" + c + "'...");

		return c == 's';
	}
	public static boolean bool(char lunedì) {
		Scanner input = new Scanner(System.in);
		String inserimento;

		do {
			errore = false;
			try {
				if(lunedì != 's' && lunedì != 'n')
					throw new Exception();
			}
			catch(Exception e) {
				JOptionPane.showMessageDialog(null,"Errore nell'inserimento. Inserire solo 's' o 'n'.");
				errore=true;
				input.reset();
			}
		}while(errore);

		JOptionPane.showMessageDialog(null,"Viene considerato il carattere '" + lunedì + "'...");

		return lunedì == 's';
	}
}
