package main;

import com.formdev.flatlaf.FlatDarkLaf;
import domain.Calendario;
import domain.Corsista;
import domain.Credenziali;
import domain.Giorno;
import domain.users.*;
import enums.GiornoSettimana;
import enums.Materia;
import utils.FileMan;
import utils.RWDomain;
import utils.Utils;
import views.*;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;
import static utils.Utils.loginWithCredentials;

public class Main {
    // File Manager per ogni file
    public static FileMan adminFile;
    public static FileMan corsiFile;
    public static FileMan docentiFile;
    public static FileMan segrFile;

    public static byte opt=-1;
    public static byte opt2=-1;
    public static byte opt3=-1;

    // Tutti i tipi di utente che possono accedere
    static Admin admin;
    static DocenteAssegnato docente;
    static Segreteria segreteria;
    static Calendario calendario;
    static ArrayList<Corsista> corsisti;
    static RegistrazioneDocente r;
    static JFrame f = new JFrame();

    public static void main(String[] args) {
        FlatDarkLaf.install();
        try {
            UIManager.setLookAndFeel( new FlatDarkLaf() );
        } catch (Exception ignored) { }
        importFiles();
        new MenuIniziale();
    }

    private static void importFiles() {
        String sep = File.separator;
        String dirPath = System.getProperty("user.dir") + sep + "data";
        File dir = new File(dirPath);
        if(!dir.isDirectory())
            dir.mkdir();
        File af = new File(dirPath + sep + "admin.hidden");
        try {
            if(af.createNewFile()){
                adminFile = new FileMan(dirPath + sep + "admin.hidden");
                adminFile.openOut(true);
                RWDomain.writeAdmin(adminFile, new Admin("admin", "CorsiRecupero",
                        new Credenziali("admin@itisandria.it", "admin10ACDFLPT")));
                adminFile.close();
            }
        } catch (IOException ignored) { }
        adminFile = new FileMan(dirPath + sep + "admin.hidden");
        corsiFile = new FileMan(dirPath + sep + "corsi.txt");
        docentiFile = new FileMan(dirPath + sep + "docenti.txt");
        segrFile = new FileMan(dirPath + sep + "segreteria.txt");
        try {
            adminFile.openOut(true); adminFile.close();
            corsiFile.openOut(true); corsiFile.close();
            docentiFile.openOut(true); docentiFile.close();
            segrFile.openOut(true); segrFile.close();
        } catch (IOException ignored) { }
    }

    public static void login() {
        LoginFrame lf = new LoginFrame("Login - Corsi di Recupero");
        lf.setVisible(false);
        if(lf.isIndietro())
            return;
        String email = lf.getEmail().getText();
        String password = String.valueOf(lf.getPassword().getPassword());

        if (password.equals("")) {
            JOptionPane.showMessageDialog(f, "Nessuna password inserita");
            Main.login();
            return;
        }
        Utente u = loginWithCredentials(email);
        if (u != null) {
            switch (u.getTipo()) {
                case ADMIN:
                    admin = (Admin) u;
                    if (password.equals(admin.getCredenziali().getPassword())) {
                        JOptionPane.showMessageDialog(f,"--- Riconosciuto Admin ---");
                        menuAdmin();
                    } else {
                        JOptionPane.showMessageDialog(f,"Password errata!");
                        Main.login();
                    }
                    break;
                case SEGRETERIA:
                    segreteria = (Segreteria) u;
                    if (password.equals(segreteria.getCredenziali().getPassword())) {
                        JOptionPane.showMessageDialog(f,"--- Riconosciuta Segreteria ---");
                        menuSegreteria();
                    } else {
                        JOptionPane.showMessageDialog(f,"Password errata!");
                        Main.login();
                    }
                    break;
                case DOCENTE:
                    docente = new DocenteAssegnato((Docente) u);
                    if (password.equals(docente.getCredenziali().getPassword())) {
                        JOptionPane.showMessageDialog(f,"--- Riconosciuto Docente ---");
                        menuDocente();
                    } else {
                        JOptionPane.showMessageDialog(f,"Credenziali errate!");
                        Main.login();
                    }
                    break;
                default: {
                    JOptionPane.showMessageDialog(f,"Email inserita inesistente.");
                    Main.login();
                }
                    break;
            }
        } else {
            JOptionPane.showMessageDialog(f,"L'email inserita non è stata trovata.");
            Main.login();
        }
    }

    public static void menuDocente() {
        MenuDocente mu = new MenuDocente();
        mu.setVisible(true);
    }

    public static void menuDocente2() {
            switch (opt3) {
                case 0:
                    System.exit(0);
                case 1:
                    docente.esportaCalendario();
                    menuDocente();
                    opt3=-1;
                    break;
            }
            System.gc(); //garbage collector
    }
    public static void menuSegreteria() {
        new MenuSegreteria();
    }

    public static void menuSegreteria2() {
            switch (opt) {
                case 0:
                    System.exit(0);
                case 1:
                    corsisti = Utils.importDataFromExcel();
                    if(corsisti != null)
                        JOptionPane.showMessageDialog(f,"Lista dei corsisti importata correttamente.");
                    else
                        JOptionPane.showMessageDialog(f,"La lista dei corsisti non è stata importata.\n" +
                                "Controllare il file Excel e riprovare.");
                    menuSegreteria();
                    opt=-1;
                    break;
                case 2:
                    if(corsisti == null)
                        JOptionPane.showMessageDialog(f,"Nessuna lista di corsisti importata.");
                    else
                        segreteria.generaCorsi(corsisti);
                    menuSegreteria();
                    opt=-1;
                    break;
                case 3:
                    calendario = segreteria.generaCalendario();
                    if(calendario != null){
                        JOptionPane.showMessageDialog(f,"Calendario generato correttamente!\n" +
                                "É possibile visualizzarlo in 'Calendario.xlsx'.");
                    }
                    menuSegreteria();
                    opt=-1;
                    break;
                case 4:
                    int id=0;
                    String idS = JOptionPane.showInputDialog(null,"ID del corso: ","data",JOptionPane.OK_OPTION);
                    try {
                        id = parseInt(idS);
                    }catch (Exception e)
                    {
                        JOptionPane.showMessageDialog(null, "Inserire un numero intero","ERRORE", JOptionPane.ERROR_MESSAGE);
                    }
                    if(id != 0)
                        segreteria.visualizzaCorso(id);
                    menuSegreteria();
                    opt=-1;
                    break;
            }
            System.gc(); //garbage collector
    }
    public static void menuAdmin() {
        new MenuAdmin();
    }

    public static void menuAdmin2() {
        String email;
        switch (opt2) {
            case 0:
                System.exit(0);
            case 1:
                cambiaPswAdmin();
                menuAdmin();
                opt2=-1;
                break;
            case 2:
                RegistrazioneSegreteria r = new RegistrazioneSegreteria();
                r.setVisible(true);
                opt2=-1;
                break;
            case 3:
                email = JOptionPane.showInputDialog(f, "Email della segreteria da modificare:","@itisandria.it");
                if(!Credenziali.isValid(email))
                    JOptionPane.showMessageDialog(f,"Inserire una email valida di dominio '@itisandria.it' .");
                admin.modificaSegreteria(email);
                opt2=-1;
                break;
            case 4:
                email = JOptionPane.showInputDialog(f, "Email del docente da modificare:","@itisandria.it");
                if(!Credenziali.isValid(email))
                    JOptionPane.showMessageDialog(f,"Inserire una email valido di dominio '@itisandria.it' .");
                admin.modificaDocente(email);
                opt2=-1;
                break;
            }
            System.gc(); //garbage collector
    }

    public static void cambiaPswAdmin() {
        ChangePswFrame pswf = new ChangePswFrame("Corsi di recupero - Cambia password"); pswf.setVisible(false);
        try{
            String vecchia = String.valueOf(pswf.getVecchiapsw().getPassword()),
                nuova = String.valueOf(pswf.getNuovapsw().getPassword()),
                ripeti = String.valueOf(pswf.getRipetipsw().getPassword());
            adminFile.openIn();
            Admin a = RWDomain.readAdmin(adminFile);
            adminFile.closeIn();
            if(a == null) {
                JOptionPane.showMessageDialog(f,"Non è stato letto alcun Admin.");
                return;
            }
            if(vecchia.equals("") || nuova.equals("")) {
                JOptionPane.showMessageDialog(f,"Almeno uno dei campi è vuoto, modifica password annullato.");
                return;
            }
            if(!vecchia.equals(a.getCredenziali().getPassword())){
                JOptionPane.showMessageDialog(f,"La vecchia password è errata, modifica password annullato.");
                return;
            }
            if(!nuova.equals(ripeti)){
                JOptionPane.showMessageDialog(f,"Le due nuove password non corrispondono, modifca password annullato.");
                return;
            }
            if(vecchia.equals(nuova)){
                JOptionPane.showMessageDialog(f,"La vecchia e la nuova password sono identiche, modifca password annullato.");
                return;
            }
            a.setCredenziali(a.getCredenziali().getEmail(), nuova);
            adminFile.openOut(false);
            RWDomain.writeAdmin(adminFile, a);
            adminFile.closeOut();
            JOptionPane.showMessageDialog(f,"Password modificata correttamente.");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(f,"Errore nell'apertura del file.");
        }
    }

    public static void nuovoDocente() {
        r = new RegistrazioneDocente();
        r.setVisible(true);
    }

    public static void nuovoDocente2(){
        ArrayList<Materia> mlist = r.getMaterie();
        boolean[] ac = r.getAnno();
        byte nc = r.getCorsi();
        boolean sl = r.getLunedi();
        GiornoSettimana fd = r.getGiorno();
        String nome= r.getNome().getText();
        String cognome= r.getCognome().getText();
        String email= r.getEmail().getText();
        String pwd = r.getPassword().getText();
        Utente u = new Utente(Utente.Type.DOCENTE, nome, cognome, new Credenziali( email, pwd));

        Docente d = new Docente(u.getNome(), u.getCognome(), u.getCredenziali(), mlist, ac, fd, nc, sl);
        try {
            docentiFile.openOut(true);
            if(!RWDomain.writeDocente(docentiFile, d))
                throw new IOException();
            JOptionPane.showMessageDialog(f,"Docente registrato correttamente.");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(f,"Errore nella scrittura nel file. Registrazione annullata.");
        }
        docentiFile.close();
        MenuIniziale mu = new MenuIniziale();
        mu.setVisible(true);
    }
}
