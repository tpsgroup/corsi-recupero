package enums;

import domain.Giorno;
import javax.swing.*;

public enum Anno {
    PRIMO, SECONDO, TERZO, QUARTO;

    public static Anno scegliAnnoCorso(byte a){
        while(true) {
            if(a >= 1 && a <= Giorno.NUMCORSI)
                return Anno.values()[a-1]; //deve andare da 0 per essere utilizzato nel vettore
            JOptionPane.showMessageDialog(null,"Inserimento non valido.");
        }
    }
}
