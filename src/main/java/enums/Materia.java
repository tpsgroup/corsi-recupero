package enums;

import javax.swing.*;

public enum Materia {
    CHIMICA, DIRITTO, DISEGNO_PROGETTO, ELETTRONICA, FISICA, INFORMATICA,
    INGLESE, ITALIANO, MATEMATICA, MECCANICA, SCIENZEDELLATERRA, SISTEMI_ELETTRONICA,
    SISTEMI_INFORMATICA, SISTEMI_MECCANICA, STORIA, TECNOLOGIE_MECCANICHE,
    TELECOMUNICAZIONI, TPS_ELETTRONICA, TPS_INFORMATICA, TTRG;

    public static Materia scegliMateria() {
        byte scelta; Object materiaObj;
        Object [] materie = {"0. Nessuna Materia Scelta","1. Chimica","2. Diritto","3. Disegno e Progetto (meccanica)","4. Elettronica","5. Fisica",
                "6. Informatica","7. Inglese","8. Italiano","9.Matematica","10. Meccanica","11. Scienze della terra","12. Sistemi (elettronica)",
                "13. Sistemi (informatica)","14. Sistemi (meccanica)", "15. Storia","16. Tecnologie meccaniche","17. Telecomunicazioni",
                "18. TPS (elettronica)","19. TPS (informatica)","20. Tecnologie e tecniche di rappresentazione grafica"};

        while(true){
            materiaObj = JOptionPane.showInputDialog(null, "Scegli una materia per volta. Premi 0 per interrompere l'inserimento delle materie","Scelta materie",JOptionPane.INFORMATION_MESSAGE,null,materie,materie[0]);
            String materiaStr = (String) materiaObj;
            char mm = materiaStr.charAt(0);
            int materiai = Character.getNumericValue(mm);
            scelta = (byte) materiai;
            Materia[] m = Materia.values();
            if(scelta == 0)
                return null;
            if(scelta >= 1 && scelta <= m.length)   // nel vettore va da 0 al numero di materie,
                return m[ scelta-1 ];               // ma la scelta inizia da 1
            JOptionPane.showMessageDialog(null,"Inserimento non valido. Ripetere.");
        }
    }
}