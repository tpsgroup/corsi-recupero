import domain.Credenziali;
import enums.Materia;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class CredenzialiTest {

    @Test
    public void isNotValidTest() {
        Materia.valueOf("Matematica");
        assertTrue(true);
        Materia.valueOf("MATEMATICA");
        assertTrue(true);
    }

    @Test
    public void isValidTest() {
        assertTrue(Credenziali.isValid("carmine.conversano@itisandria.it"));
    }



}
