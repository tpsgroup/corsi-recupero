# CORSI RECUPERO #

Si vuole automatizzare la gestione dei corsi di recupero del nostro Istituto.<br>
Si realizzi il modello UML del sistema considerando che in input un docente può:<br>
* inserire la sua disponibilità a tenere uno o più corsi;
* specificare il suo giorno libero (da escludere per il calendario);
* specificare la materia e l’anno di riferimento (primo, secondo, ecc).

In output si deve produrre il calendario che ottemperi alle seguenti direttive:
* evitare sovrapposizioni di corsi per una data classe;
* evitare il corso nella giornata libera del docente che tiene un determinato corso;
* evitare che lo stesso docente tenga più corsi nello stesso giorno.

Gli utenti che utilizzano il sistema sono:
* docenti (inseriscono disponibilità e consultano calendario);
* segreteria didattica (inserisce gli elenchi dei corsisti per ogni corso specificando materia ed anno).

**STEP 1**: <br>
Disegnare il Diagramma delle Classi e quello dei Casi d’uso (Class Diagram, Use case Diagram).<br>
**STEP 2**: <br>
realizzare la procedura in Java.